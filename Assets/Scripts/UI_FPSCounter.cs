﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class UI_FPSCounter : MonoBehaviour
{
    // From http://wiki.unity3d.com/index.php?title=FramesPerSecond#CSharp_HUDFPS.cs

    // Attach this to a GUIText to make a frames/second indicator.
    //
    // It calculates frames/second over each updateInterval,
    // so the display does not keep changing wildly.
    //
    // It is also fairly accurate at very low FPS counts (<10).
    // We do this not by simply counting frames per interval, but
    // by accumulating FPS for each frame. This way we end up with
    // correct overall FPS even if the interval renders something like
    // 5.5 frames.

    public float updateInterval = 0.5F;

    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval
    
    Text hudText;

    Color Orange = new Color(1f, 0.6471f, 0, 1);

    void Start()
    {
        hudText = GetComponentInChildren<Text>();

        if (!hudText)
        {
            Debug.Log("FPSCount needs a GUIText component!");
            enabled = false;
            return;
        }
        timeleft = updateInterval;
        UpdateCounter();
    }

    void Update()
    {
        timeleft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        if (timeleft <= 0.0)
        {
            UpdateCounter();
        }
    }

    void UpdateCounter()
    {
        // display two fractional digits (f2 format)
        float fps = accum / frames;
        string format = $"{fps:F1} FPS";
        hudText.text = format;

        if (fps < 60)
            hudText.color = Orange;
        else if (fps < 30)
            hudText.color = Color.yellow;
        else if (fps < 24)
            hudText.color = Color.red;
        else if (fps < 15)
            hudText.color = Color.white;
        else
            hudText.color = Color.green;
        // DebugConsole.Log(format,level);
        timeleft = updateInterval;
        accum = 0.0F;
        frames = 0;
    }
}
