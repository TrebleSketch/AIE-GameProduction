﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerStats : MonoBehaviour
{
    public static UI_PlayerStats instance;
    public PlayerStats localPlayerStats;

    List<GameObject> ui_statsList = new List<GameObject>();

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        for (int stats = 0; stats < transform.childCount; stats++)
        {
            ui_statsList.Add(transform.GetChild(stats).gameObject);
        }
    }

    public void FirstLoadStatsUI()
    {
        // This is run when the stats is set, but the UI haven't been updated yet
        for (int uiStats = 0; uiStats < ui_statsList.Count; uiStats++)
        {
            GameObject ui_object = ui_statsList[uiStats];
            Text ui_statsNumber = ui_object.transform.Find("Value").GetComponent<Text>();
            Transform ui_statsBar = ui_object.transform.Find("Bar");
            // C#7
            //Stats stats;
            //Enum.TryParse(ui_object.name, out Stats stats);
            Stats.StatsTypes stats = (Stats.StatsTypes)Enum.Parse(typeof(Stats.StatsTypes), ui_object.name, true);

            // Just in case that the value isn't the max, have it still do the same
            switch (stats)
            {
                case Stats.StatsTypes.Health:
                    ui_statsNumber.text = localPlayerStats.c_Health.Value.ToString();
                    ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Health.MaxValue, localPlayerStats.c_Health.Value), 1, 1);
                    break;
                case Stats.StatsTypes.Hunger:
                    ui_statsNumber.text = localPlayerStats.c_Hunger.Value.ToString();
                    ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Hunger.MaxValue, localPlayerStats.c_Hunger.Value), 1, 1);
                    break;
                case Stats.StatsTypes.Warmth:
                    ui_statsNumber.text = localPlayerStats.c_Warmth.Value.ToString();
                    ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Warmth.MaxValue, localPlayerStats.c_Warmth.Value), 1, 1);
                    break;
                case Stats.StatsTypes.Alertness:
                    ui_statsNumber.text = localPlayerStats.c_Alertness.Value.ToString();
                    ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Alertness.MaxValue, localPlayerStats.c_Alertness.Value) , 1, 1);
                    break;
                case Stats.StatsTypes.Stamina:
                    ui_statsNumber.text = localPlayerStats.c_Stamina.Value.ToString();
                    ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Stamina.MaxValue, localPlayerStats.c_Stamina.Value), 1, 1);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(stats), stats, null);
            }
        }
    }

    public void UpdateStatsUI(Stats.StatsTypes stats, float value, bool isActualValue)
    {
        if (stats != Stats.StatsTypes.Health &&
            stats != Stats.StatsTypes.Hunger &&
            stats != Stats.StatsTypes.Warmth &&
            stats != Stats.StatsTypes.Alertness &&
            stats != Stats.StatsTypes.Stamina)
        {
            if (stats == Stats.StatsTypes.None)
                Debug.LogWarning("Unable to update UI as the type is set to 'None', please try again");

            return;
        }

        GameObject ui_object = transform.Find(stats.ToString()).gameObject;
        Text ui_statsNumber = ui_object.transform.Find("Value").GetComponent<Text>();
        Transform ui_statsBar = ui_object.transform.Find("Bar");

        switch (stats)
        {
            case Stats.StatsTypes.Health:
                //float newHealthNum = localPlayerStats.c_Health.Value;//ValueChange(localPlayerStats.c_Health.Value, value, isActualValue);

                ui_statsNumber.text = ((int)localPlayerStats.c_Health.Value).ToString();
                ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Health.MaxValue, localPlayerStats.c_Health.Value), 1, 1);
                break;
            case Stats.StatsTypes.Hunger:
                //float newHungerNum = localPlayerStats.c_Hunger.Value;//ValueChange(localPlayerStats.c_Hunger.Value, value, isActualValue);

                //Debug.Log(localPlayerStats.c_Hunger.Value + " | " + value + " | " + isActualValue);
                //print(localPlayerStats.c_Hunger.Value);

                ui_statsNumber.text = ((int)localPlayerStats.c_Hunger.Value).ToString();
                ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Hunger.MaxValue, localPlayerStats.c_Hunger.Value), 1, 1);
                break;
            case Stats.StatsTypes.Warmth:
                //float newWarmthNum = localPlayerStats.c_Warmth.Value;//ValueChange(localPlayerStats.c_Warmth.Value, value, isActualValue);

                ui_statsNumber.text = ((int)localPlayerStats.c_Warmth.Value).ToString();
                ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Warmth.MaxValue, localPlayerStats.c_Warmth.Value), 1, 1);
                break;
            case Stats.StatsTypes.Alertness:
                //float newAlertnessNum = localPlayerStats.c_Alertness.Value;//ValueChange(localPlayerStats.c_Alertness.Value, value, isActualValue);

                ui_statsNumber.text = ((int)localPlayerStats.c_Alertness.Value).ToString();
                ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Alertness.MaxValue, localPlayerStats.c_Alertness.Value), 1, 1);
                break;
            case Stats.StatsTypes.Stamina:
                ui_statsNumber.text = ((int)localPlayerStats.c_Stamina.Value).ToString();
                ui_statsBar.localScale = new Vector3(UIBarPercentage(localPlayerStats.c_Stamina.MaxValue, localPlayerStats.c_Stamina.Value), 1, 1);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(stats), stats, null);
        }
    }

    // This changes the size of the green bar
    float UIBarPercentage(float maxValue, float currentValue)
    {
        if (currentValue > maxValue)
            Debug.LogError("UI_PlayerStats: Value given is larger than max value");
        else if (currentValue < 0)
            Debug.LogError("UI_PlayerStats: Value given is less than 0");
        return currentValue / maxValue;
    }
}
