﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class InventorySquare : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    protected UI_GUI uiInventory;

    // This is the 'Inventory' Manager, but it's called Inventory...
    [HideInInspector] public Inventory inventory;

    public bool isCraftingOutput;

    public Item item;

    public Image ItemIcon;
    //[SerializeField] GameObject ItemInfoPopupPrefab;

    GameObject ItemInfoPopupGO = null;
    Text popup_ItemName => UI_InventoryInfoPopup.instance.ItemName;
    Text popup_ItemType => UI_InventoryInfoPopup.instance.ItemType;
    Text popup_ItemDescription => UI_InventoryInfoPopup.instance.ItemDescription;

    public void OnPointerEnter (PointerEventData eventData)
    {
        if (item != null) SpawnPopup();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ToggleItemWithClicks();
    }

    public void OnPointerExit (PointerEventData eventData)
    {
        if (item == null) return;
        if (ItemInfoPopupGO != null)
        {
            Destroy(ItemInfoPopupGO);
        }
    }





    void Awake()
    {
        ItemIcon = transform.Find("Image").GetComponent<Image>();
        inventory = GetComponentInParent<Inventory>();
    }


    void OnEnable()
    {
        if (inventory != null)
        {
            if (inventory.InventoryItems.Count == inventory.InventorySize && inventory.InventorySquares.Count == inventory.InventorySize)
            {
                Item ItemInList =
                        inventory.InventoryItems[inventory.InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == this)];

                if (item != ItemInList)
                {
                    item = ItemInList;
                    UpdateInventorySquareSprite();
                }
            }
        }
    }


    void Start()
    {
        uiInventory = UI_GUI.instance;
        UpdateInventorySquareSprite();
    }

    void LateUpdate()
    {
        if (ItemInfoPopupGO == null) return;
        ItemInfoPopupGO.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
    }




    public void UseItem(uint mouseButtonClicked)
    {
        if (item == null) return;
        
        // Weapons have their own mouse click logic
        // Here is the general logic
        // It is public as this would generally be disabled when using items
        if (item.ItemType != Item.ItemTypes.BodyArmour &&
            item.ItemType != Item.ItemTypes.Helmet &&
            item.ItemType != Item.ItemTypes.LargeWeapon &&
            item.ItemType != Item.ItemTypes.LegArmour &&
            item.ItemType != Item.ItemTypes.MediumWeapon &&
            item.ItemType != Item.ItemTypes.SmallWeapon)
        {
            FoodItem foodItem = null;

            if (item.ItemType == Item.ItemTypes.Food || item.ItemType == Item.ItemTypes.Drink)
                foodItem = (FoodItem)item;

            if (mouseButtonClicked == 1)
            {
                Debug.Log("Right clicked on item");
                return;
            }

            if (foodItem != null)
                if (foodItem.IsThisAnEmptyItem)
                {
                    // logic to refill the item will be here
                    return;
                }

            item.Use();

            // If the item is perishable, then remove the itself from the InventorySquare that is is residing in after use.
            // If it's able to be used
            if (item.IsPerishable && item.ShouldItemBeUsed)
            {
                switch (item.ItemType)
                {
                    case Item.ItemTypes.PlaceableObjects:
                        if (BuildingManager.instance.ItemsAbleToBeBuilt.ContainsKey(item))
                            UsingUpItem();
                        break;
                    case Item.ItemTypes.Food:
                    case Item.ItemTypes.Drink:
                        UsingUpItem();
                        break;
                }
            }
        }
        else
            Debug.LogWarning("Item was not used as you are selecting a weapon");
    }

    void UsingUpItem()
    {
        FoodItem foodItem = null;

        if (item.ItemType == Item.ItemTypes.Food || item.ItemType == Item.ItemTypes.Drink)
            foodItem = (FoodItem)item;

        // have it checked if it can be used
        if (foodItem != null)
        {
            if (foodItem.ItemAfterUse != null)
                item = foodItem.ItemAfterUse;
        }
        else
            item = null;
        UpdateInventorySquareSprite();

        if (inventory != null)
            inventory.UpdateInventoryWithNewData(this, item);

        //Debug.Log("Item is now used up!");
    }

    protected void SpawnPopup()
    {
        //Debug.Log("Spawning Popup, focusing on InventorySquare spawning first");
        ItemInfoPopupGO = Instantiate(uiInventory.InventorySquareInfoPopup, uiInventory.transform, false);
        ItemInfoPopupGO.name = ItemInfoPopupGO.name.Replace("(Clone)", "");

        //ItemInfoPopupGO.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1f);

        //ItemInfoPopupGO = Instantiate(CreatePopUp());
        UpdatePopupWithData();
    }

    //GameObject CreatePopUp()
    //{
    //    Debug.Log("Spawning item pop up!");

    //    // Create a popup from scratch to reduce prefab
    //    GameObject prefab = new GameObject("Mouse-Over Inventory Info Panel");
    //    prefab.transform.SetParent(UI_Inventory.instance.transform);

    //    Vector3 halfScale = new Vector3(0.5f, 0.5f, 1f);

    //    // Update the Rect Transform
    //    RectTransform prefabRect = prefab.AddComponent<RectTransform>();
    //    prefabRect.sizeDelta = new Vector2(700f, 560f);
    //    prefabRect.localScale = halfScale;
    //    prefabRect.pivot = new Vector2(-0.04f, 1.04f);

    //    // Create Children GO
    //    GameObject nameGO = new GameObject("Item Name");
    //    GameObject typeGO = new GameObject("Item Type");
    //    GameObject descriptionGO = new GameObject("Item Description");
    //    nameGO.transform.SetParent(prefab.transform);
    //    typeGO.transform.SetParent(prefab.transform);
    //    descriptionGO.transform.SetParent(prefab.transform);

    //    // Adding Rect Transform
    //    nameGO.AddComponent<RectTransform>();
    //    typeGO.AddComponent<RectTransform>();
    //    descriptionGO.AddComponent<RectTransform>();


    //    // Update the Rect Transform
    //    RectTransform[] childrenRect = prefab.GetComponentsInChildren<RectTransform>();
    //    for (int child = 0; child < childrenRect.Length; child++)
    //    {
    //        if (childrenRect[child] == prefabRect)
    //            continue;

    //        childrenRect[child].anchorMin = Vector2.up;
    //        childrenRect[child].anchorMax = Vector2.up;
    //        childrenRect[child].localScale = halfScale;

    //        Vector2 size = Vector2.zero;
    //        Vector3 position = Vector3.zero;

    //        switch (childrenRect[child].name)
    //        {
    //            case "Item Name":
    //                size = new Vector2(650f, 80f);
    //                position = new Vector3(15f, -15f, 0);
    //                break;
    //            case "Item Type":
    //                size = new Vector2(650f, 60f);
    //                position = new Vector3(15f, -60f, 0);
    //                break;
    //            case "Item Description":
    //                size = new Vector2(650f, 330f);
    //                position = new Vector3(15f, -10f, 0);
    //                break;
    //            default:
    //                Debug.LogError("More data for Item Popup Box has been added, please set the size and position!", childrenRect[child].gameObject);
    //                break;
    //        }

    //        childrenRect[child].sizeDelta = size;
    //        childrenRect[child].localPosition = position;
    //    }

    //    // Adding classes/components
    //    prefab.AddComponent<CanvasRenderer>();
    //    Image prefabImage = prefab.AddComponent<Image>();
    //    prefabImage.color = new Color(81f/255f, 221f/255f, 221f/255f, 225f/255f);
        
    //    // Set Text components
    //    popup_ItemName = nameGO.AddComponent<Text>();
    //    popup_ItemType = typeGO.AddComponent<Text>();
    //    popup_ItemDescription = descriptionGO.AddComponent<Text>();

    //    // Update Text
    //    popup_ItemName.fontSize = 60;
    //    popup_ItemName.color = Color.black;

    //    popup_ItemType.fontSize = 40;
    //    popup_ItemType.alignment = TextAnchor.MiddleLeft;
    //    popup_ItemType.color = Color.black;

    //    popup_ItemDescription.fontSize = 50;
    //    popup_ItemDescription.horizontalOverflow = HorizontalWrapMode.Wrap;
    //    popup_ItemDescription.color = new Color(69f / 255f, 69f / 255f, 69f / 255f, 1f);

    //    return prefab;
    //}

    void UpdatePopupWithData()
    {
        popup_ItemName.text = item.Name;
        popup_ItemType.text = item.ItemType.ToString();
        popup_ItemDescription.text = item.Description;
    }

    protected void UpdateInventorySquareSprite()
    {
        if (item == null)
        {
            ItemIcon.color = new Color(1, 1, 1, 0);
            //Debug.LogWarning("No item set in this inventory square, sorry.", this);
            
            return;
        }
        // Removes item from list, when removing item from here
        // inventory.InventoryItems.Insert(inventory.InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == this), null);
        // Adding item in
        // inventory.InventoryItems.Insert(inventory.InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == this), item);

        //Debug.Log("Sprite Updated", this);

        ItemIcon.color = Color.white;

        if (item.Icon != null)
            ItemIcon.sprite = item.Icon;
        else
            Debug.LogWarning("This item does not have an icon set!", this);

        //if (inventory != null)
        //    inventory.UpdateInventoryWithNewData(this, item);


        // Update Item listing in 'Inventory'

        // Find "this" InventorySquare in "Inventory.InventorySquares"
        // Get its listing ID
        //inventory.InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == this);

        // This removes an item
        //inventory.InventoryItems.RemoveAt(inventory.InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == this));
        // This is getting the item variable stored in InventorySquares list
        //inventory.InventoryItems[inventory.InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == this);


        // Use that listing ID to either
        // > Remove it from the list
        // > Add it to the list

    }

    public void SpawnItemFromCrafting (Item craftedItem)
    {
        // Future function
        if (!isCraftingOutput)
        {
            Debug.LogWarning("This Inventory Square is not configured to work as a crafting output, " +
                             "please check the 'isCraftingOutput' box in the Editor. Thank you.", this);
            return;
        }
        if (craftedItem == null)
        {
            Debug.LogWarning("The Item you have wanted to craft is null, please try again. Thank you.", this);
        }

        // Set the local item as this
        item = craftedItem;

        // Update the icon
        UpdateInventorySquareSprite();

        // And, I think that's it?
    }

    public void GivePlayerItemFromAction (Item givenItem)
    {
        if (isCraftingOutput)
        {
            Debug.LogWarning("This Inventory Square is configured to work as a crafting output, " +
                             "please check the 'isCraftingOutput' box in the Editor. Thank you.", this);
            return;
        }
        if (givenItem == null)
        {
            Debug.LogWarning("The Item you have wanted to give is null, please try again. Thank you.", this);
        }

        // Set the local item as this
        item = givenItem;

        // Update the icon
        UpdateInventorySquareSprite();

        if (inventory != null)
            inventory.UpdateInventoryWithNewData(this, item);
    }

    protected virtual void ToggleItemWithClicks()
    {
        if (item != null)
        {
            // This prevented from Items that is being held from being deleted and overridden
            if (uiInventory.MouseHoldingItem != null)
                return;

            // This removes the item from the square and onto the Inventory Manager
            if (ItemInfoPopupGO != null)
                Destroy(ItemInfoPopupGO);

            uiInventory.MouseHoldingItem = item;
            // "Transfer" item icon over to hover over mouse
            UI_GUI.instance.UpdateFloatingIcon(uiInventory.MouseHoldingItem.Icon);
            item = null;
        }
        else
        {
            if (uiInventory.MouseHoldingItem == null) return;
            if (isCraftingOutput) return; // This disallows players to put items into the crafting output box

            // This adds the item to the square and removes item from Inventory Manager
            item = uiInventory.MouseHoldingItem;
            uiInventory.MouseHoldingItem = null;
            // Removes item icon from Mouse
            UI_GUI.instance.UpdateFloatingIcon(null);

            if (ItemInfoPopupGO == null)
                SpawnPopup();
        }

        UpdateInventorySquareSprite();

        if (inventory != null)
            inventory.UpdateInventoryWithNewData(this, item);
    }
}
