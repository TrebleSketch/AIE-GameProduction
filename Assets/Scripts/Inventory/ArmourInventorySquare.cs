﻿public class ArmourInventorySquare : InventorySquare
{
    // this is to be used for Armour Inventory Squares, special logic in here!
    public Item.ItemTypes AllowedItem1 = Item.ItemTypes.None;
    public Item.ItemTypes AllowedItem2 = Item.ItemTypes.None;
    public Item.ItemTypes AllowedItem3 = Item.ItemTypes.None;

    protected override void ToggleItemWithClicks()
    {
        if (uiInventory.MouseHoldingItem == null)
        {
            if (item != null)
            {
                // This is when item is being removed
                base.ToggleItemWithClicks();

                // Stats will change
                // Check for item effects
                UI_ArmourAttire.instance.UpdatePlayerArmourStats();
            }
        }
        // Check if it is allowed before setting the item in the square
        else if (uiInventory.MouseHoldingItem.ItemType == AllowedItem1 ||
            uiInventory.MouseHoldingItem.ItemType == AllowedItem2 ||
            uiInventory.MouseHoldingItem.ItemType == AllowedItem3)
        {
            // This is when item is being placed
            base.ToggleItemWithClicks();

            // Stats will change
            UI_ArmourAttire.instance.UpdatePlayerArmourStats();
        }
        //Debug.LogWarning("This item is not allowed in here!");
    }

    // depending on which InventorySquare that this is from, it will change different type of data
    // Linked with UI_ArmourAttire as it is linked with the nessasary classes

    void ArmourStatsChanged(float changeValue, bool isAddingArmour)
    {
        //UI_PlayerStats.instance.localPlayerStats
    }
}
