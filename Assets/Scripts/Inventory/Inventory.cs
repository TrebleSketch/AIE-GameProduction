﻿using System;
using System.Collections.Generic;
using UnityEngine;

// This class goes on every block of Inventory Space

// Copy / paste from InventorySpace script
// This script just handles the logic for how inventory items are sorted, inputs for items,
// and how inventory items are moved between inventory spaces.
// Like Player's inventory, Other Player's Inventory, Chest inventory, and Craft output
public class Inventory : MonoBehaviour
{
    public bool SourceInventory;
    public DateTime LastUpdate;
    DateTime localSync;

    public string InventoryName => inventoryName;
    [SerializeField] string inventoryName;

    public int InventorySize => inventorySize;
    [SerializeField] int inventorySize;

    public List<InventorySquare> InventorySquares = new List<InventorySquare>();
    public List<Item> InventoryItems = new List<Item>();

    public List<Inventory> LinkedInventories = new List<Inventory>();

    public List<InventorySquare> SpecificSquaresToUpdate = new List<InventorySquare>();

    void Awake()
    {
        //Debug.Log("Populating the List with Data");
        // populate the lists with data
        AddInventoryDataToLists(GetComponentsInChildren<InventorySquare>());
    }

    public void ManualAwake(DateTime manualSync)
    {
        //Debug.Log("Going to manually enable | " + name, this);
        if (InventoryItems.Count == InventorySize && InventorySquares.Count == InventorySize)
            return;
        AddInventoryDataToListsViaManualAwake(manualSync, GetComponentsInChildren<InventorySquare>());
        //Debug.Log("Should be manually enabled | " + name, this);
    }

    void OnEnable()
    {
        // This is called when enabling the UI
        // There is a chance that there is no data, so check if there are squares in InventorySquares and InventoryItems first.
        // Populate the two lists with the data!
        // Then check which other "Inventory" in the list has the time closest with now

        localSync = DateTime.Now;

        if (LinkedInventories.Count > 0)
        {
            // Then checks which LastUpdate is closest to now and then update.

            if (LinkedInventories.Count > 1)
            {
                List<DateTime> linkedInventoriesTimes = new List<DateTime>();

                for (int inventories = 0; inventories < LinkedInventories.Count; inventories++)
                {
                    if (LinkedInventories[inventories].InventoryItems.Count != LinkedInventories[inventories].InventorySize &&
                        LinkedInventories[inventories].InventorySquares.Count != LinkedInventories[inventories].InventorySize)
                        LinkedInventories[inventories].ManualAwake(localSync);
                    linkedInventoriesTimes.Add(LinkedInventories[inventories].LastUpdate);
                }
                // Check out .FindAll
                int index = linkedInventoriesTimes.FindIndex(x => x <= DateTime.Now);

                if (InventoryItems != LinkedInventories[index].InventoryItems)
                {
                    // If update is the same as the latest update, don't do anything
                    if (LastUpdate != LinkedInventories[index].LastUpdate)
                    {
                        // If the time is behind this, update all the ones that are behind
                        if (LastUpdate > LinkedInventories[index].LastUpdate)
                        {
                            Debug.LogWarning("this inventory's LastUpdate is ahead of the latest updated on of the LinkedInventories, updating all other to be like this one. " +
                                      "This is just a debug message, nothing is happening yet.", this);
                        }
                        else
                        {
                            InventoryItems = LinkedInventories[index].GetInventory(localSync, this, true);
                        }
                    }
                }
            }
            else
            {
                // Check if it needs to be manually Awoken! OWO
                if (LinkedInventories[0].InventoryItems.Count != LinkedInventories[0].InventorySize &&
                    LinkedInventories[0].InventorySquares.Count != LinkedInventories[0].InventorySize)
                    LinkedInventories[0].ManualAwake(localSync);

                // If there are exactly same items, no need to update
                if (InventoryItems != LinkedInventories[0].InventoryItems)
                {
                    // If the LastUpdate is the same. Meaning it was not manually Awoken.
                    if (LastUpdate != LinkedInventories[0].LastUpdate)
                    {
                        // If this inventory is ahead, update the rest.
                        if (LastUpdate > LinkedInventories[0].LastUpdate)
                        {
                            //Debug.Log("this inventory's LastUpdate is ahead of the only other one, updating the other inventory to be like this one. " +
                            //          "This is just a debug message, nothing is happening yet.", this);

                            // This inventory is the source inventory and it is the perfect size
                            // The LinkedInventory isn't a source inventory, this inventory
                            // This LinkedInventory also isn't the same size
                            if (!LinkedInventories[0].SourceInventory)
                            // If the two Inventories aren't the same size and the 
                            {
                                LinkedInventories[0].SetInventory(localSync, GetInventory(localSync, LinkedInventories[0], true));
                            }
                            // The Inventory IS a source Inventory
                            else
                            {
                                InventoryItems = LinkedInventories[0].GetInventory(localSync, this, true);
                            }
                        }
                        // LastUpdate < LinkedInventories[0].LastUpdate
                        // When the LastUpdate is behind the LinkedInventory
                        else
                        {
                            // Past this means the LastUpdate is less than the Linked Inventory's LastUpdate
                            // Meaning it is behind
                            // While also not having the same InventoryItems

                            // Check if it is a special inventory, as it would always have less than what the original inventory have
                            // Have to make a special check later on


                            if (LinkedInventories[0].SourceInventory)
                            {
                                if (LinkedInventories[0].InventorySize == InventorySize)
                                    InventoryItems = LinkedInventories[0].GetInventory(localSync, this, true);
                            }
                            // LinkedInventory isn't the SourceInventory
                            else
                            {
                                LinkedInventories[0].InventoryItems = GetInventory(localSync, LinkedInventories[0], true);
                            }
                        }
                    }
                }
            }
            LastUpdate = localSync;
        }
    }

    void AddInventoryDataToLists(InventorySquare[] squaresToAdd)
    {
        if (InventorySquares.Count != 0) InventorySquares.Clear();
        if (InventoryItems.Count != 0) InventoryItems.Clear();

        localSync = DateTime.Now;

        // Add all the InventorySquare objects into list
        InventorySquares.AddRange(squaresToAdd);
        // Add the Item variable into list
        for (int i = 0; i < InventorySquares.Count; i++)
        {
            InventorySquares[i].inventory = this;
            InventoryItems.Add(InventorySquares[i].item);
        }

        // Called original
        LastUpdate = localSync;
    }

    void AddInventoryDataToListsViaManualAwake(DateTime manualSync, InventorySquare[] squaresToAdd)
    {
        if (InventorySquares.Count != 0) InventorySquares.Clear();
        if (InventoryItems.Count != 0) InventoryItems.Clear();

        // Add all the InventorySquare objects into list
        InventorySquares.AddRange(squaresToAdd);
        // Add the Item variable into list
        for (int i = 0; i < InventorySquares.Count; i++)
        {
            InventorySquares[i].inventory = this;
            InventoryItems.Add(InventorySquares[i].item);
        }

        // Sync with the one manually syncing it
        LastUpdate = manualSync;
    }

    public void UpdateInventoryWithNewData(InventorySquare square, Item newData)
    {
        //Debug.Log("Updating...", this);
        // Updating sync time
        localSync = DateTime.Now;

        // Updating the Lists
        InventoryItems.RemoveAt(InventorySquares.FindIndex(obj => obj == square));
        InventoryItems.Insert(InventorySquares.FindIndex(obj => obj == square), newData);

        // Update the copies
        if (LinkedInventories.Count <= 0)
        {
            LastUpdate = localSync;
            return;
        }

        int indexPosition = InventorySquares.FindIndex(obj => obj.GetComponent<InventorySquare>() == square);

        foreach (Inventory copies in LinkedInventories)
        {
            // If there is special squares, run through this as the copy will most likely NOT be the same size
            // Make sure that both Specific Squares list are the same size
            if (copies.InventorySize != InventorySize)
            {
                if (SpecificSquaresToUpdate.Count != copies.SpecificSquaresToUpdate.Count)
                {
                    if (copies.InventorySize == InventorySize)
                    {
                        Debug.LogWarning("The two inventories that you want to link can't be linked as they have two different sizes. " +
                                 "The current invetory has got  " + InventorySize + " slots and the one copying to has got " +
                                 copies.InventorySize + " slots.");
                        return;
                    }
                    Debug.LogWarning("The SpecificSquares inventory that you want to link can't be linked as they have two different sizes. " +
                        "The current SpecificSquares inventory has got " + SpecificSquaresToUpdate.Count + " slots and the one copying to has got " +
                        copies.SpecificSquaresToUpdate.Count + " slots.");
                    return;
                }

                for (int specificSquare = 0; specificSquare < SpecificSquaresToUpdate.Count; specificSquare++)
                {
                    // Find the InventorySquares to be changed
                    //copies.SpecificSquaresToUpdate[specificSquare];
                    if (square == SpecificSquaresToUpdate[specificSquare])
                        copies.UpdateInventoryWithSquarePosition(specificSquare, newData);
                }
                //Debug.Log("Updates for special square");
            }
            // If no specific squares that needs to be updated, just allow it
            else if (copies.InventorySize == InventorySize)
                copies.UpdateInventoryWithSquarePosition(indexPosition, newData);

            copies.LastUpdate = localSync;
        }

        // Called on the original
        LastUpdate = localSync;
    }

    public void UpdateInventoryWithSquarePosition(int listPosition, Item newData)
    {
        if (listPosition > InventoryItems.Count || listPosition > InventorySquares.Count)
        {
            Debug.LogWarning("List Index is out of index, please try again.", this);
            return;
        }
        if (listPosition < 0)
        {
            Debug.LogWarning("List Index is negative, please try again.", this);
            return;
        }

        // Disabling the Square to refresh it
        InventorySquares[listPosition].enabled = false;

        // insert the updated item
        InventoryItems[listPosition] = newData;

        // Turning it back on
        InventorySquares[listPosition].enabled = true;
    }

    public void SetInventory(DateTime syncTime, List<Item> items)
    {
        // Checks if the inventory size is exactly the same, if not, return an error
        if (items.Count != InventoryItems.Count)
        {
            Debug.LogWarning("Can't set the Inventory as the one providing isn't the same size.");
            return;
        }

        // disable every InventorySquare
        ToggleInventorySquares(false);

        // Add the Items
        InventoryItems = items;

        // Renable to "reload" the InventorySquares
        ToggleInventorySquares(true);

        LastUpdate = syncTime;
    }

    public List<Item> GetInventory (DateTime syncTime, Inventory fetchingInventory, bool fromWithinSpeciicInventory)
    {
        if (fromWithinSpeciicInventory && fetchingInventory != this)
        {
            if (fetchingInventory.InventorySize != InventorySize)
            {
                List<Item> SendingInventory = new List<Item>();
                for (int i = 0; i < SpecificSquaresToUpdate.Count; i++)
                {
                    int index = InventorySquares.FindIndex(obj => obj == SpecificSquaresToUpdate[i]);
                    SendingInventory.Add(InventoryItems[index]);
                }
                return SendingInventory;
            }
            return InventoryItems;
        }

        // If they are not the right size, it is most likely that they are a SpecificSquares Inventory
        if (fetchingInventory.InventorySize != InventorySize)
        {
            // If the one wanting to fetch isn't the same as this, this would imply that it's a mistake and cancel this.
            if (fetchingInventory.SpecificSquaresToUpdate.Count != SpecificSquaresToUpdate.Count)
            {
                Debug.LogWarning("The two inventories that you want to fetch can't be fetched as they have two different sizes. " +
                                "The fetching invetory has got  " + InventorySize + " slots and the one trying to fetch has got " +
                                fetchingInventory.InventorySize + " slots.");
                return null;
            }
            //Debug.Log("getting a special inventory - " + name, this);
            LastUpdate = syncTime;
            return InventoryItems;
        }
        //Debug.Log("getting a normal inventory - " + name, this);
        LastUpdate = syncTime;
        return InventoryItems;
    }

    void ToggleInventorySquares(bool isEnabled)
    {
        foreach (var toggle in InventorySquares)
        {
            toggle.enabled = isEnabled;
        }
    }
}
