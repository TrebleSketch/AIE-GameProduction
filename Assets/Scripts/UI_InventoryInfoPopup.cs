﻿using UnityEngine;
using UnityEngine.UI;

public class UI_InventoryInfoPopup : MonoBehaviour
{
    public static UI_InventoryInfoPopup instance;

    [SerializeField] Text m_itemName;
    public Text ItemName => m_itemName;

    [SerializeField] Text m_itemType;
    public Text ItemType => m_itemType;

    [SerializeField] Text m_itemDecription;
    public Text ItemDescription => m_itemDecription;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
}
