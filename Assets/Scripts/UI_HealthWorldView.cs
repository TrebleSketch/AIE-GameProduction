﻿using UnityEngine;
using UnityEngine.UI;

public class UI_HealthWorldView : MonoBehaviour
{
    EnemyHealth m_EnemyHealth;
    PlayerHealth m_PlayerHealth;
    bool isEnemyHealthBar;
    bool isScriptEnvironmentRight = true;

    public GameObject UIPrefab;

    GameObject uiGO;
    Transform WorldHealthBarTransform;
    public Transform LocalPlayerCamera;

    bool shouldUIBeVisible = true;

    float barPercentage
    {
        get { return barPercentageActual; }
        set
        {
            if (value > 1f && barPercentageActual < 1f)
            {
                Debug.LogWarning("Health Bar Percentage can not be more than 1. Automatically setting it to 1.");
                barPercentageActual = 1f;
            }
            else if (value < 0f && barPercentageActual > 0f)
            {
                Debug.LogWarning("Health Bar Percentage can not be less than 0. Automatically setting it to 0.");
                barPercentageActual = 0f;
            }
            else if (value <= 1f && value >= 0f)
                barPercentageActual = value;
        }
    }
    float barPercentageActual;

    void Awake()
    {
        m_EnemyHealth = GetComponent<EnemyHealth>();
        m_PlayerHealth = GetComponent<PlayerHealth>();

        if (m_EnemyHealth != null && m_PlayerHealth == null)
            isEnemyHealthBar = true;
        else if (m_EnemyHealth == null && m_PlayerHealth != null)
            isEnemyHealthBar = false;
        else if (m_EnemyHealth == null && m_PlayerHealth == null)
        {
            Debug.LogWarning("UI_HealthWorldView doesn't have any health scripts to run, please put in a script!", this);
            isScriptEnvironmentRight = false;
        }
        else
            isScriptEnvironmentRight = false;
    }

    void Start()
    {
        if (!isScriptEnvironmentRight) return;

        // Spawns the prefab
        uiGO = Instantiate(UIPrefab, transform, false);
        uiGO.name = uiGO.name.Replace("(Clone)", "");

        WorldHealthBarTransform = uiGO.transform.Find("Background").Find("Health");
    }

    void Update()
    {
        if (!isScriptEnvironmentRight) return;

        // Sets up the direction to face towards
        if (UI_PlayerStats.instance.localPlayerStats != null && LocalPlayerCamera == null)
        {
            LocalPlayerCamera = UI_PlayerStats.instance.localPlayerStats.GetComponentInChildren<Camera>().transform;
            if (LocalPlayerCamera == null)
                print("no luck at finding camera");
        }

        if (LocalPlayerCamera != null)
        {
            if (Vector3.Distance(transform.position, LocalPlayerCamera.position) > 18 && shouldUIBeVisible)
                shouldUIBeVisible = false;
            else if (Vector3.Distance(transform.position, LocalPlayerCamera.position) < 18 && !shouldUIBeVisible)
                shouldUIBeVisible = true;

            //print("Distance: " + Vector3.Distance(transform.position, LocalPlayerCamera.position));
        }

        if (uiGO.GetComponent<Canvas>().enabled != shouldUIBeVisible)
            uiGO.GetComponent<Canvas>().enabled = shouldUIBeVisible;
        
        if (!shouldUIBeVisible)
            return;
        
        WorldHealthBarTransform.localScale = new Vector3(UpdateBarPercentage(), 1f, 1f);
    }

    float UpdateBarPercentage()
    {
        barPercentage = isEnemyHealthBar ?
            m_EnemyHealth.Health / m_EnemyHealth.MaxHealth :
            m_PlayerHealth.Value / m_PlayerHealth.MaxValue;
        return barPercentage;
    }
}
