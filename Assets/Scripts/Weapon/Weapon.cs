﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    // This script is to be placed on every single weapon. Small, medium, and large.
    // This will hold the logic for shooting, damage, blocking, and other things.

    public WeaponItem WeaponData;

    RaycastHit hit;


    public void Use()
    {

    }

    public void RightClick()
    {

    }
}
