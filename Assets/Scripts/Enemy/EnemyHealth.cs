﻿using UnityEngine;

[RequireComponent(typeof(UI_HealthWorldView))]
public class EnemyHealth : MonoBehaviour
{

    public float Health => m_health;
    float m_health
    {
        get
        {
            return m_actualHealth;
        }
        set
        {
            if (value > m_maxHealth)
                Debug.Log("Setting health to a value higher than max health, please do it again. Thanks!", this);
            else
                m_actualHealth = value;
        }
    }
    [SerializeField] float m_actualHealth;
    public float MaxHealth => m_maxHealth;
    [SerializeField] float m_maxHealth;


    public void FirstSetHealth(float health)
    {
        m_maxHealth = health;
        m_health = m_maxHealth;
    }

    public void AddHealth(float healing)
    {
        if (healing <= 0)
            Debug.LogWarning("Healing done needs to be a positive float.", this);
        m_health += healing;
    }

    public void DeductHealth(float damage)
    {
        if (damage <= 0)
            Debug.LogWarning("Damage done needs to be a positive float.", this);
        else if (m_health <= 0)
            Debug.LogWarning("Enemy is already dead, not allowed to reduce health anymore.", this);
        m_health -= damage;

        // Check if the enemy is dead
        if (m_health <= 0)
        {
            m_health = 0;
            Invoke(nameof(Death), 2f);
        }
    }

    void Death()
    {
        // Remove self from the list
        GetComponentInParent<EnemySpawner>().ListOfEnemies.RemoveAt(GetComponentInParent<EnemySpawner>().ListOfEnemies.FindIndex(obj => gameObject));
        Debug.Log("DEATH!");
        Destroy(gameObject);
    }
}
