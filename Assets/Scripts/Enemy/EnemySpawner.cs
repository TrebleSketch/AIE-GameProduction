﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{
    // Enemy can be spawned in waves or one-at-a-time.

    public enum SpawnType
    {
        None,
        Waves,
        OneEach
    }

    EnemyManager m_EnemyManager;

    // Enemy to spawn
    public EnemyManager.Enemies EnemyToSpawn;

    // Check if a specific location is needed
    public bool SpawnsInSpecificLocation;

    // Specific Position to Spawn the enemy
    public Vector3 SpawnPosition;

    // This chooses each type of spawner
    public SpawnType SpawnerTypes = SpawnType.OneEach;

    // Enemy AI States, each AI is to be inputted with it!
    public EnemyAIStates StartAIStates = EnemyAIStates.None;

    // The usage is dependent on what spawner type
    // Types ? true : false
    // Waves ? multiple waves seperated by "SpawnPeriod" in seconds : single wave
    // OneEach ? time between spawn is the length of "SpawnPeriod" : single
    public bool isTimed = true;

    // Amount that is spawned each time
    // Max 4 at a time rn
    public uint AmountEachSpawn = 1;

    // Between each spawn (waves & one each)
    public float SpawnPeriod = 3f;
    
    // If the spawner will start immediately
    public bool StartsImmediately;

    // Wait for this many seconds before you start
    public float TimeToStart = 2f;

    // Maximum number of enemies that is allowed
    public int MaximumAmountOfEnemies = 6;
    
    // List of patrol points, will be used if needed
    public List<Transform> ListOfPatrolPoints = new List<Transform>();

    // Enemies that are spawned will be placed here
    public List<GameObject> ListOfEnemies = new List<GameObject>();

    void Awake()
    {
        if (SpawnerTypes == SpawnType.None)
            Debug.LogWarning("This enemy spawner needs a type!", this);
    }

    void Start()
    {
        if (SpawnerTypes == SpawnType.None) return;

        m_EnemyManager = EnemyManager.instance;

        if (SpawnerTypes == SpawnType.OneEach)
            AmountEachSpawn = 1;

        // Spawn at beginning here
        if (isTimed)
            InvokeRepeating(nameof(SpawnEnemies), StartsImmediately ? 0 : TimeToStart, SpawnPeriod);
        else
            SpawnEnemies();
    }

    void SpawnEnemies()
    {
        if (ListOfEnemies.Count >= MaximumAmountOfEnemies)
        {
            //print("max amount of spawned enemies reached");
            return;
        }

        if (AmountEachSpawn > 1)
        {
            if (AmountEachSpawn > 4)
            {
                Debug.LogError("There are too many spawning each time. Currently only supporting spawning no more than 4 enemies at a time.", this);
                return;
            }

            // Spawns in a 2 by 2 formation

            // For multiple spawning
            for (int i = 0; i < AmountEachSpawn; i++)
            {
                GameObject newEnemies = Instantiate(m_EnemyManager.EnemyPrefabs[EnemyToSpawn], transform, false);
                newEnemies.name = newEnemies.name.Replace("(Clone)", " - #0" + (ListOfEnemies.Count + 1));
                
                // Position matters. Rotation not so much.
                newEnemies.transform.position = SpawnsInSpecificLocation ? SpawnPosition : transform.position;

                switch (i)
                {
                    case 0:
                        newEnemies.transform.position = new Vector3(
                            newEnemies.transform.position.x,
                            newEnemies.transform.position.y + newEnemies.transform.localScale.y,
                            newEnemies.transform.position.z);
                        break;
                    case 1:
                        newEnemies.transform.position = new Vector3(
                            newEnemies.transform.position.x + 2f, 
                            newEnemies.transform.position.y + newEnemies.transform.localScale.y, 
                            newEnemies.transform.position.z);
                        break;
                    case 2:
                        newEnemies.transform.position = new Vector3(
                            newEnemies.transform.position.x, 
                            newEnemies.transform.position.y + newEnemies.transform.localScale.y, 
                            newEnemies.transform.position.z - 2);
                        break;
                    case 3:
                        newEnemies.transform.position = new Vector3(
                            newEnemies.transform.position.x + 2f, 
                            newEnemies.transform.position.y + newEnemies.transform.localScale.y, 
                            newEnemies.transform.position.z - 2f);
                        break;
                }

                // calculate the Y-pos of the terrain that they should be standing on.
                RaycastHit position;
                Physics.Raycast(transform.position, Vector3.down, out position, 10f);
                



                newEnemies.GetComponent<EnemyLogic>().AIInitializeOnSpawn(StartAIStates);
                newEnemies.GetComponent<EnemyLogic>().m_spawner = this;

                ListOfEnemies.Add(newEnemies);
            }
            return;
        }
        // Single spawning

        GameObject newEnemy = Instantiate(m_EnemyManager.EnemyPrefabs[EnemyToSpawn], transform, false);
        newEnemy.name = newEnemy.name.Replace("(Clone)", " - #0" + (ListOfEnemies.Count + 1));

        // Position matters. Rotation not so much.
        newEnemy.transform.position = SpawnsInSpecificLocation ? SpawnPosition : transform.position;

        // Making sure that it is touching the surface
        // todo: In future, check for where the surface is and then spawn them on to the surface
        newEnemy.transform.position = new Vector3(
            newEnemy.transform.position.x,
            newEnemy.transform.position.y + newEnemy.transform.localScale.y,
            newEnemy.transform.position.z);
        //print(newEnemy.transform.localScale.y);

        newEnemy.GetComponent<EnemyLogic>().AIInitializeOnSpawn(StartAIStates);
        newEnemy.GetComponent<EnemyLogic>().m_spawner = this;

        // Places enemy into the list
        ListOfEnemies.Add(newEnemy);
    }
}
