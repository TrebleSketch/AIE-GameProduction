﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

// This scriptable object will contain data for Enemies
[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy/An Enemy")]
[Serializable]
public class Enemy : ScriptableObject
{
    // Enemy name
    public string Name;

    // Enemy Description
    public string Description;
    
    // Maximum Health
    public float MaxHealth;

    // Movement Speed
    public float MovementSpeed;

    // Attack damage
    public float damage;

    // Attack delays
    public float delay;
}
