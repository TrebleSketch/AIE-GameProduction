﻿using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public enum EnemyAIStates
{
    None,
    Pause,
    Roaming_Random,
    Patrol,
    Chase,
    Attack
}

[RequireComponent(typeof(EnemyHealth))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyLogic : MonoBehaviour
{
    [HideInInspector]
    public EnemySpawner m_spawner;

    public Enemy EnemyData;
    EnemyHealth m_health;

    public bool IsAIActive;

    NavMeshAgent m_navMeshAgent;

    public EnemyAIStates CurrentAIState => AIState;
    [SerializeField] EnemyAIStates AIState = EnemyAIStates.None;

    Vector3 PositionOnTheFloor
    {
        get
        {
            RaycastHit position;
            Physics.Raycast(transform.position, Vector3.down, out position, 2f);
            return position.point;
        }
    }


    public void AIInitializeOnSpawn(EnemyAIStates startState)
    {
        AIState = startState;
    }

    void StartAIBeginning()
    {
        IsAIActive = true;
        switch (AIState)
        {
            case EnemyAIStates.Pause:
                ChangeToWait();
                break;
            case EnemyAIStates.Roaming_Random:
                ChangeToRoamingRandom();
                break;
            case EnemyAIStates.Patrol:
                ChangeToPatrol();
                break;
            case EnemyAIStates.Chase:
                ChangeToChase();
                break;
            case EnemyAIStates.Attack:
                ChangeToAttack();
                break;
        }
    }



    void Awake()
    {
        if (EnemyData == null)
        {
            Debug.LogError("This enemy needs data to run!", this);
            return;
        }

        m_health = GetComponent<EnemyHealth>();
        m_navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        if (EnemyData == null) return;
        if (m_spawner == null)
        {
            Debug.LogError("No Spawner is attached to this enemy...", this);
            return;
        }

        m_health.FirstSetHealth(EnemyData.MaxHealth);
        m_navMeshAgent.speed = EnemyData.MovementSpeed;

        // No need to set state as the Spawner sets the initial state
        //AILogicUpdate();
        int randStart = Random.Range(0, 4);
        //print("random delay is: " + randStart + " seconds");
        Invoke(nameof(StartAIBeginning), randStart);

        // Updates transform to be transfixed on the Player's Camera
        playerToChase = Camera.main.transform;
    }

    void Update()
    {
        if (EnemyData == null || !IsAIActive || AIState == EnemyAIStates.None || m_spawner == null) return;

        AILogicUpdate();
    }


    // AI Functions/Logic

    protected void AILogicUpdate()
    {
        switch (AIState)
        {
            case EnemyAIStates.Pause:
                Pause();
                break;
            case EnemyAIStates.Roaming_Random:
                RoamingRandom();
                break;
            case EnemyAIStates.Patrol:
                Patrol();
                break;
            case EnemyAIStates.Chase:
                Chase();
                break;
        }
    }


    // AI variables here
    //[SerializeField] List<Transform> PatrollingPositions = new List<Transform>();

    // Only one transform that you need to check out for as it's only singleplayer for now.
    // Revise this variable for multiplayer implementation
    Transform playerToChase;


    void ChangeToWait()
    {
        AIState = EnemyAIStates.Pause;

        m_navMeshAgent.isStopped = true;

        int randStart = Random.Range(2, 7);
        Invoke(nameof(ChangeToPatrol), randStart);
    }

    void Pause()
    {
        if (Vector3.Distance(transform.position, playerToChase.position) < 6)
        {
            ChangeToChase();
        }
    }

    int patrolListIndex;

    void ChangeToPatrol()
    {
        // Make sure that the AI can move
        m_navMeshAgent.isStopped = false;

        // Return to normal speed
        m_navMeshAgent.speed = EnemyData.MovementSpeed;

        // resets the index to wherever the bot is the closest to
        List<float> closestPoint = new List<float>();
        for (int i = 0; i < m_spawner.ListOfPatrolPoints.Count; i++)
        {
            // adds the distance between the AI and all the other patrol points
            closestPoint.Add(Vector3.Distance(transform.position, m_spawner.ListOfPatrolPoints[i].position));
        }
        //closestPoint.Min(pos => pos.)
        // Source: https://stackoverflow.com/questions/3820025/select-only-the-lowest-values-with-linq
        //int min = (int)closestPoint.Min(entry => entry);
        //var lowestValues = closestPoint.Where(entry => entry == closestPoint.Min(check => check));

        if (closestPoint.Count == m_spawner.ListOfPatrolPoints.Count)
            patrolListIndex = closestPoint.IndexOf(closestPoint.Min(check => check));
        else
        {
            Debug.Log("very vague error, please check this out", this);
            patrolListIndex = 0;
        }

        // Set the position to move to
        m_navMeshAgent.SetDestination(m_spawner.ListOfPatrolPoints[patrolListIndex].position);

        // Set to be in patrol state
        AIState = EnemyAIStates.Patrol;
    }

    void Patrol()
    {
        // checks if there are enough patrol points
        if (m_spawner.ListOfPatrolPoints.Count < 1)
        {
            Debug.LogWarning("Please set more than one patrol points, where is this bot going to talk to?");
            return;
        }

        // if too close to the nearest patrol point, move to the next patrol point
        if (Vector3.Distance(PositionOnTheFloor, m_spawner.ListOfPatrolPoints[patrolListIndex].position) < 0.5f)
        {
            patrolListIndex++;
            // looping between the points
            if (patrolListIndex >= m_spawner.ListOfPatrolPoints.Count)
            {
                patrolListIndex = 0;
            }
            // Set the next point
            m_navMeshAgent.SetDestination(m_spawner.ListOfPatrolPoints[patrolListIndex].position);
        }

        // If close to the player, then start chasing the player >:D
        // todo: In the future, have a trigger in front of the enemy as the FOV. When player enters it, start chasing
        if (Vector3.Distance(transform.position, playerToChase.position) < 6.5f)
        {
            ChangeToChase();
        }
    }

    void ChangeToRoamingRandom()
    {
        m_navMeshAgent.speed = EnemyData.MovementSpeed;
    }

    void RoamingRandom()
    {

    }

    void ChangeToChase()
    {
        // When chasing the player, the speed increases slightly
        AIState = EnemyAIStates.Chase;
        m_navMeshAgent.isStopped = false;

        m_navMeshAgent.speed = EnemyData.MovementSpeed * 1.8f;
    }

    bool allowedToAttack = true;

    void Chase()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, playerToChase.position);

        if (distanceToPlayer > 1.1f)
            m_navMeshAgent.SetDestination(playerToChase.position);
        else
        {
            Vector3 direction = Vector3.RotateTowards(transform.position, (playerToChase.position - transform.position), 60f, 0);
            transform.rotation = new Quaternion(0, Quaternion.LookRotation(direction).y, 0, Quaternion.LookRotation(direction).w);

            m_navMeshAgent.SetDestination(transform.position);
        }


        // If enemy is close enough to player, do attack please
        if (allowedToAttack)
        {
            // Get close to the player
            if (distanceToPlayer < 2.4f)
                ChangeToAttack();
        }

        // Change this to be when the player moves out of the enemy's FOV, with a trigger in the direction where the enemy is facing
        if (distanceToPlayer > 12f)
        {
            ChangeToWait();
        }
    }

    void ChangeToAttack()
    {
        // Now you are not allowed to attack
        allowedToAttack = false;

        // Change to attack mode
        AIState = EnemyAIStates.Attack;

        // Current AI will stop, wait for 0.4 seconds and then attack the player
        m_navMeshAgent.isStopped = true;

        Invoke(nameof(Attack), 0.4f);
    }

    void Attack()
    {
        // Attack!
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 5f))
        {
            if (hit.collider.CompareTag("Player"))
            {
                hit.collider.GetComponentInParent<PlayerHealth>().Deduct(EnemyData.damage);
            }
        }

        // After about 1 second after the attack/attempted attack, the enemy will return to Chase mode and continue chasing the player
        Invoke(nameof(ChangeToChase), 1f);
        Invoke(nameof(ReturnAllowedToAttackValue), EnemyData.delay);
    }

    void ReturnAllowedToAttackValue()
    {
        allowedToAttack = true;
    }
}
