﻿using System;
using UnityEngine;

public class Stats : MonoBehaviour
{
    [Serializable]
    public enum StatsTypes
    {
        None,
        Health,
        Hunger,
        Warmth,
        Alertness,
        Armour,
        Stamina
    }

    public StatsTypes StatsType = StatsTypes.None;

    protected PlayerStats c_stats => UI_PlayerStats.instance.localPlayerStats;
    
    public float Value => m_value;
    public float MaxValue => m_MaxValue;

    protected float m_value
    {
        get { return m_ActualValue; }
        set
        {
            // todo: check Enemy Health's range checks and fix this to be as good as that
            if (value <= m_MaxValue && value >= 0)
                m_ActualValue = value;
            else
                Debug.LogWarning("Trying to set " + StatsType + " value out of range!", this);
        }
    }
    float m_ActualValue;

    protected float m_MaxValue
    {
        get { return m_ActualMaxValue; }
        set
        {
            if (value > 1)
                m_ActualMaxValue = value;
            else
                Debug.LogWarning("Trying to set " + StatsType + " max value below 1!", this);
        }
    }
    float m_ActualMaxValue;

    protected float m_Multiplier
    {
        get { return m_ActualMultiplier; }
        set
        {
            if (value > 0)
                m_ActualMultiplier = value;
            else
                Debug.LogWarning("Trying to set " + StatsType + " value multiplier below 0!", this);
        }
    }
    float m_ActualMultiplier = 1f;


    // Add Stat Value
    public void Add(float addition)
    {
        if (addition <= 0)
            Debug.LogWarning("Addition to " + StatsType + " needs to be done with a positive float.", this);

        m_value += (addition * m_Multiplier);

        UpdateUI(addition, false);
    }

    // Deduct Stat Value
    public void Deduct(float deduction)
    {
        if (deduction <= 0)
            Debug.LogWarning("Deduction to " + StatsType + " needs to be done with a positive float.", this);
        else if (m_value <= 0)
            Debug.LogWarning(StatsType + " is already zero, not allowed to reduce the " + StatsType + " anymore.", this);

        float deductionCalc = deduction * m_Multiplier;

        if (deductionCalc > m_value)
            m_value -= m_value;
        else
            m_value -= deductionCalc;

        UpdateUI(deduction, false);
    }

    // Please use this to set up, not to increase or decrease due to Effects
    public void Set (float value, bool isSetUpValues)
    {
        if (m_value == 0)
           SetMax(value, isSetUpValues);
        m_value = value;
        if (!isSetUpValues)
            UpdateUI(m_value, true);
    }

    public void SetMax(float value, bool isSetUpValues)
    {
        m_MaxValue = value;
        if (!isSetUpValues)
            UpdateUI(m_value, true);
    }

    void UpdateUI(float value, bool isActualValue)
    {
        //print("updating UI");
        UI_PlayerStats.instance.UpdateStatsUI(StatsType, value, isActualValue);
    }
}
