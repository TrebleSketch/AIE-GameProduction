﻿using System;
using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Item", menuName = "Item/An Item")]
[Serializable]
public class Item : ScriptableObject
{
    public enum ItemTypes
    {
        None,
        // Body protection items
        Helmet,
        BodyArmour,
        LegArmour,
        // Weapons and what not
        LargeWeapon,
        MediumWeapon,
        SmallWeapon,
        // Stuff that you use
        Food,
        Drink,
        Resource,
        // Placeable Objects
        PlaceableObjects,
        // This for types idk what to do for them yet
        Miscellaneous
    }

    public enum ItemUsageEffectTiming
    {
        None,
        Instant,
        Repeating
    }



    // Item GUID
    public string Guid;

    // Name
    public string Name;
    
    // Type of Item
    public ItemTypes ItemType;

    // Description
    public string Description;

    // Image representation of Item
    // (Large and scaled down for small versions)
    public Sprite Icon;

    // If the item is perishable, meaning it will disappear when used
    public bool IsPerishable;

    // This is a check for all items, it will always be true. Unless something pops up
    public bool ShouldItemBeUsed = true;
    


    // Whether the item will have an effect immediately or over a period of time
    public ItemUsageEffectTiming EffectTiming = ItemUsageEffectTiming.Instant;

    // Time is always in seconds

    // Time between each effect
    public uint TimeInterval;

    // Time for how many times an effect repeats
    public uint EffectRepeatingAmount;



    // List of Items that's needed to make this item
    public List<CraftingRequirement> CraftingRequirements = new List<CraftingRequirement>();

    // List of GameObjects that can be spawned
    public List<GameObject> ItemsToSpawn = new List<GameObject>();

    // List of Effects this item will affect the player
    public List<StatsEffect> StatsEffects = new List<StatsEffect>();




    public void Print()
    {
        Debug.Log(Name + " | " + ItemType + " | " + Description);
    }

    // This is called publically to use the item
    public virtual void Use()
    {
        ShouldItemBeUsed = true;

        // Run the effects of the item
        // Since there is nothing in the Effects function in this class, nothing will happen.
        // If it is overridden in another class, something will run.
        Effects();

        // If it is a placeable object, also spawn the object
        if (ItemType == ItemTypes.PlaceableObjects && ItemsToSpawn.Count > 0)
        {
            //Debug.Log("Spawn object!");
            
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (BuildingManager.instance.ItemsAbleToBeBuilt.ContainsKey(this))
                {
                    BuildingManager.instance.BuildItem(this, hit.point, ItemsToSpawn[0].transform.rotation);
                }
            }
        }
    }

    protected virtual void Effects() { }

    public virtual void MultiplierEffects() { }
}

[Serializable]
public class CraftingRequirement
{
    public Item item;
    public int numberRequired;
}