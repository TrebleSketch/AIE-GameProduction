﻿using UnityEngine;
using UnityEngine.UI;

public class ItemToCraft : MonoBehaviour
{
    // Have it public for now... Maybe?
    public Item item;

    [SerializeField] Image ItemIcon;
    [SerializeField] Text ItemName;
    [SerializeField] Text ItemTypeAndResourcesRequired;

    void Start()
    {
        if (item != null)
            SetUp();
        else
            GetComponent<Button>().interactable = false;
    }

    public void SetUp()
    {
        if (item == null)
        {
            Debug.LogError("Why are you setting Crafting Square without having an item set first???", this);
            return;
        }
        ItemIcon.color = Color.white;
        ItemIcon.sprite = item.Icon;
        ItemName.text = item.Name;
        ItemTypeAndResourcesRequired.text = item.ItemType.ToString();

        if (item.CraftingRequirements.Count != 0)
            TypeAndResourcesText();
        else
            ItemTypeAndResourcesRequired.text += " | No resources required";
    }

    public void SetUp(Item newItem)
    {
        item = newItem;
        ItemIcon.color = Color.white;
        ItemIcon.sprite = item.Icon;
        ItemName.text = item.Name;
        ItemTypeAndResourcesRequired.text = item.ItemType.ToString();

        if (item.CraftingRequirements.Count != 0)
            ItemTypeAndResourcesRequired.text = TypeAndResourcesText();
        else
            ItemTypeAndResourcesRequired.text += " | No resources required";
    }

    string TypeAndResourcesText()
    {
        string resourceText = "";
        for (int num = 0; num < item.CraftingRequirements.Count; num++)
        {
            if (item.CraftingRequirements.Count < 2)
            {
                return $"{item.CraftingRequirements[num].item.Name} (x{item.CraftingRequirements[num].numberRequired})";
            }

            if (num == 1)
            {
                resourceText =
                $"{item.CraftingRequirements[num].item.Name} (x{item.CraftingRequirements[num].numberRequired})";
                continue;
            }

            string addString =
                $", {item.CraftingRequirements[num].item.Name} (x{item.CraftingRequirements[num].numberRequired})";

            resourceText += addString;
        }
        return resourceText;
    }

    // I think this is self explainatory
    public void Craft()
    {
        //Debug.Log("Crafting!");

        // Use up resources if there are any to be used up
        // Requires setting up PlayerInventory and InventorySpace

        UI_Crafting.instance.CraftingOutput.SpawnItemFromCrafting(item);
    }

    // todo: Enable and disable the button depending on if there are enough resources in the Player's Inventory
}
