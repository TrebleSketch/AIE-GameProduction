﻿using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Food Item", menuName = "Item/Food Item")]
[Serializable]
public class FoodItem : Item
{
    // After the food item is used, it turns into this item
    public Item ItemAfterUse;

    // If item is an empty item, don't use this.
    public bool IsThisAnEmptyItem;

    protected override void Effects()
    {
        // There isn't anything in the base function so far
        //base.Effects();
        

        // This is for ItemUsage
        if (EffectTiming == ItemUsageEffectTiming.Instant)
        {
            StatsUse();
        }
        else if (EffectTiming == ItemUsageEffectTiming.Repeating)
        {
            // This is not possible in ScriptableObjects, gotta find a way
            Debug.LogWarning("Not able to repeat effect yet, this is still a WIP. (" + Name + ")", this);
        }
    }

    float CheckIfNearLimit(float statsChanged, float currentStat, float maxStat)
    {
        float absolute = Mathf.Abs(statsChanged);
        if (statsChanged < 0) // deducting health
        {
            if (currentStat == 0)
            {
                ShouldItemBeUsed = false;
                return 0;
            }
            // if the StatsChange is more than the current stat, just return the current stat but minus.
            // To have it return to zero.
            if (absolute >= currentStat)
                return -currentStat;
            return absolute;
        }
        if (statsChanged > 0) // adding health
        {
            if (currentStat == maxStat)
            {
                ShouldItemBeUsed = false;
                return 0;
            }
            // if the StatsChange is more than the max state, just return what's left between current & max stat
            // So have the stats be full
            if ((absolute + currentStat) >= maxStat)
                return maxStat - currentStat;
            return absolute;
        }
        ShouldItemBeUsed = false;
        return 0;
    }

    void StatsUse()
    {
        foreach (var t in StatsEffects)
        {
            if (t.StatsChange > 0)
            {
                switch (t.StatsToEffect)
                {
                    case Stats.StatsTypes.Health:
                        UI_PlayerStats.instance.localPlayerStats.c_Health.Add(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Health.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Health.MaxValue));
                        break;
                    case Stats.StatsTypes.Hunger:
                        UI_PlayerStats.instance.localPlayerStats.c_Hunger.Add(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Hunger.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Hunger.MaxValue));
                        break;
                    case Stats.StatsTypes.Warmth:
                        UI_PlayerStats.instance.localPlayerStats.c_Warmth.Add(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Warmth.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Warmth.MaxValue));
                        break;
                    case Stats.StatsTypes.Alertness:
                        UI_PlayerStats.instance.localPlayerStats.c_Alertness.Add(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Alertness.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Alertness.MaxValue));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else if (t.StatsChange < 0)
            {
                switch (t.StatsToEffect)
                {
                    case Stats.StatsTypes.Health:
                        UI_PlayerStats.instance.localPlayerStats.c_Health.Deduct(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Health.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Health.MaxValue));
                        break;
                    case Stats.StatsTypes.Hunger:
                        UI_PlayerStats.instance.localPlayerStats.c_Hunger.Deduct(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Hunger.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Hunger.MaxValue));
                        break;
                    case Stats.StatsTypes.Warmth:
                        UI_PlayerStats.instance.localPlayerStats.c_Warmth.Deduct(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Warmth.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Warmth.MaxValue));
                        break;
                    case Stats.StatsTypes.Alertness:
                        UI_PlayerStats.instance.localPlayerStats.c_Alertness.Deduct(
                            CheckIfNearLimit(
                                t.StatsChange, 
                                UI_PlayerStats.instance.localPlayerStats.c_Alertness.Value, 
                                UI_PlayerStats.instance.localPlayerStats.c_Alertness.MaxValue));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }

    //IEnumerator RepeatEffect()
    //{
    //    for (uint amountLeft = EffectRepeatingAmount; amountLeft == 0; amountLeft--)
    //    {
    //        StatsChangeCheck();
    //        yield return new WaitForSeconds(EffectRepeatingAmount);
    //    }
    //}
}
