﻿using UnityEngine;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Armour Item", menuName = "Item/Armour Item")]
[Serializable]
public class ArmourItem : Item
{
    // Variables of the armour that will affect player's stat multiplier
    // List of Effects this item will affect the player
    public StatsEffect ArmourStatsEffect = new StatsEffect();

    public int GetArmourValue() => ArmourStatsEffect.StatsChange;

    //protected override void Effects()
    //{
    //    // There isn't anything in the base function so far
    //    //base.Effects();
    //}
}
