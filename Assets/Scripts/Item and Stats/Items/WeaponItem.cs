﻿using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Weapon Item", menuName = "Item/Weapon Item")]
[Serializable]
public class WeaponItem : Item
{
    public enum Type
    {
        Projectile,
        Blade
    }

    // Add additional weapon effects in the future

    // The type of Weapon
    public Type WeaponType;

    // Amount of damage this weapon inflicts
    // in HP
    public int Damage;

    // Amount of time between each damage infliction
    // in seconds
    public float Delay;

    // Maximum distance away from player where damage may be inflicted
    // in Unity units
    public float Distance;

    //protected override void Effects()
    //{
    //    // There isn't anything in the base function so far
    //    //base.Effects();
    //}

    public void SpawnWeapon()
    {
        // Should I put it in here or somewhere else

        // Spawn the weapon
        //GameObject weapon = Instantiate();
        // Put the data of this item in the weapon

    }

    public override void Use()
    {
        // Shoot a raycast out and damage
        Debug.Log("Pew!");

        // Effects() will be run in Use()
        base.Use();
    }

    // When weapon is clicked on the right, this happens
    public void RightClick()
    {
        Debug.Log("Right click!");
    }
}
