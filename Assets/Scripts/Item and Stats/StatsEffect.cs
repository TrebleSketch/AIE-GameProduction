﻿using UnityEngine;
using System;

[Serializable]
public class StatsEffect
{
    // This class will be in a list
    // Effect/s that an object will do on the four stats

    // How much should the Stats be changed by each time
    public int StatsChange;

    // The Types of Effect that will be affected
    public Stats.StatsTypes StatsToEffect = Stats.StatsTypes.None;
}
