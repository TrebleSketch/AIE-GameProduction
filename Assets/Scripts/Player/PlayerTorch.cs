﻿using UnityEngine;

public class PlayerTorch : MonoBehaviour
{
    [SerializeField] Light m_theLight;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
            m_theLight.enabled = !m_theLight.enabled;
    }
}
