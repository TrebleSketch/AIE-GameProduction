﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class PlayerInteracting : MonoBehaviour
{
    // Major understanding of this concept obtained from here
    // https://answers.unity.com/questions/650308/how-do-i-interact-with-terrain-trees.html
    public Terrain terrain;
    List<TreeInstance> m_treeInstance;
    List<TreeInstance> m_treeInstanceOriginal;

    GameObject uiSpawned;

    RaycastHit hit;

    int treeIDX = -1;
    int treeCount = 0;
    float treeDist = 2f;
    Vector3 uiPos = Vector3.zero;

    void Start()
    {
        if (terrain != null)
        {
            m_treeInstance = terrain.terrainData.treeInstances.ToList();
            m_treeInstanceOriginal = terrain.terrainData.treeInstances.ToList();
        }
    }

    void Update()
    {
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 5f))
        {
            // Terrain = Tree or something
            if (hit.collider.GetComponent<Terrain>() == null)
            {
                //print("Found an object : \"" + hit.transform.name + "\" - distance: " + hit.distance);
                if (uiSpawned != null)
                    UI_WorldSpaceInteractions.instance.RemoveUI(uiSpawned);
                return;
            }

            // Tree cutting distance is below 2 units
            if (hit.distance > 3f) return;

            float groundHeight = terrain.SampleHeight(hit.point);

            if (hit.point.y - 0.3f > groundHeight)
            {
                if (uiSpawned == null)
                {
                    CheckingForTree();
                    uiSpawned = UI_WorldSpaceInteractions.instance.GenerateUI(
                        UI_WorldSpaceInteractions.UIType.Tree, 
                        new Vector3(uiPos.x, uiPos.y + 0.6f, uiPos.z));
                }
                if (Input.GetKeyDown(KeyCode.Q))
                    ObtainingTree();
            }
        }
        else
        {
            if (uiSpawned != null)
                UI_WorldSpaceInteractions.instance.RemoveUI(uiSpawned);
        }
    }

    void CheckingForTree()
    {
        treeIDX = -1;
        treeCount = m_treeInstance.Count;
        treeDist = 2f;

        // Notice we are looping through every terrain tree, 
        // which is a caution against a 15,000 tree terrain

        for (int cnt = 0; cnt < treeCount; cnt++)
        {
            Vector3 thisTreePos = Vector3.Scale(m_treeInstance[cnt].position, terrain.terrainData.size) + terrain.transform.position;
            float thisTreeDist = Vector3.Distance(thisTreePos, hit.point);

            if (thisTreeDist < treeDist)
            {
                uiPos = Vector3.Scale(m_treeInstance[cnt].position, terrain.terrainData.size);
                treeIDX = cnt;
                treeDist = thisTreeDist;
            }
        }

        // something is breaking, this isn't working any more after moving it out of ObtainingTree
        if (treeIDX > -1) return;

        Debug.Log("Out of Range");
    }

    // idk where to put it but here
    void ObtainingTree()
    {
        // Marking that it is obtained
        //GameObject marker = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //marker.transform.position = treePos;

        // now give the player the item

        // Let the tree "disappear"
        m_treeInstance.RemoveAt(treeIDX);
        terrain.terrainData.treeInstances = m_treeInstance.ToArray();

        // Refresh collider
        float[,] heights = terrain.terrainData.GetHeights(0, 0, 0, 0);
        terrain.terrainData.SetHeights(0, 0, heights);

        // Find the item first, I don't know how to use Linq or stuff to find the name of the item
        Item wood = null;

        for (int search = 0; search < CraftingManager.instance.ItemsAllowed.Count; search++)
        {
            if (CraftingManager.instance.ItemsAllowed[search].Name == "Wood")
                wood = CraftingManager.instance.ItemsAllowed[search];
        }

        // Give player the item
        if (wood != null)
            UI_PlayerOnHandInventory.instance.GivePlayerItemAndAnIsAbleCheck(wood);
        else
            print("unable to give player wood, can't find it");

        Invoke(nameof(ReturnTree), 5);
    }

    void ReturnTree()
    {
        terrain.terrainData.treeInstances = m_treeInstanceOriginal.ToArray();
        m_treeInstance = terrain.terrainData.treeInstances.ToList();

        float[,] heights = terrain.terrainData.GetHeights(0, 0, 0, 0);
        terrain.terrainData.SetHeights(0, 0, heights);
    }
}
