﻿using UnityEngine;

public class PlayerArmour : Stats
{
    // This value is what each player starts with
    // Can be changes respective to which character is chosen (future)
    public int BaseValue => m_BaseValue;
    int m_BaseValue;

    public int ArmourValue => m_ArmourValue;
    int m_ArmourValue;

    // Armour value determains how muh damage is taken when attacked

    void Awake()
    {
        // Sets it here for now, as there isn't Character selection
        //SetBaseValue(20);
        m_BaseValue = 20;
        m_ArmourValue = m_BaseValue;
    }

    //void SetBaseValue(int value)
    //{        
    //    m_BaseValue = value;
    //}

    public void UpdateArmourValue(int totalValue)
    {
        m_ArmourValue = totalValue;
    }

    // Set multiplier
    // This is to reduce damage done when attacked
}
