﻿using UnityEngine;
using System;

[RequireComponent(typeof(PlayerHealth))]
[RequireComponent(typeof(PlayerHunger))]
[RequireComponent(typeof(PlayerWarmth))]
[RequireComponent(typeof(PlayerAlertness))]
[RequireComponent(typeof(PlayerArmour))]
public class PlayerStats : MonoBehaviour
{
    UI_PlayerStats ui;

    public PlayerHealth c_Health => c_health;
    public PlayerHunger c_Hunger => c_hunger;
    public PlayerWarmth c_Warmth => c_warmth;
    public PlayerAlertness c_Alertness => c_alertness;
    // Stats that aren't always on the screen
    public PlayerArmour c_Armour => c_armour;
    // Dynamic Stats
    public PlayerStamina c_Stamina => c_stamina;
    

    PlayerHealth c_health;
    PlayerHunger c_hunger;
    PlayerWarmth c_warmth;
    PlayerAlertness c_alertness;
    // Stats that aren't always on the screen
    PlayerArmour c_armour;
    // Dynamic stats
    PlayerStamina c_stamina;



    void Awake()
    {
        c_health = GetComponent<PlayerHealth>();
        c_hunger = GetComponent<PlayerHunger>();
        c_warmth = GetComponent<PlayerWarmth>();
        c_alertness = GetComponent<PlayerAlertness>();
        c_armour = GetComponent<PlayerArmour>();
        c_stamina = GetComponent<PlayerStamina>();
    }

    void Start()
    {
        ui = UI_PlayerStats.instance;
        ui.localPlayerStats = this;

        // Set the max value
        c_health.Set(100, true);
        c_hunger.Set(100, true);
        c_warmth.Set(100, true);
        c_alertness.Set(100, true);
        c_stamina.Set(120, true);

        // then load the ui
        ui.FirstLoadStatsUI();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Shelter") && BiomesManager.instance != null)
            BiomesManager.instance.CurrentBiome.IsSafe = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Shelter") && BiomesManager.instance != null)
            BiomesManager.instance.CurrentBiome.IsSafe = false;
    }
}
