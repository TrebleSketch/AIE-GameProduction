﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlayerWeapon : MonoBehaviour
{
    public WeaponItem WeaponOnHand
    {
        get
        {
            InventorySquare checkItem = UI_PlayerOnHandInventory.instance.CurrentWeaponSelection;

            if (checkItem != null)
            {
                if (m_currentWeapon != checkItem.item)
                    WeaponChangeInVariable(checkItem);
            }
            else if (m_currentWeapon != null)
                WeaponChangeInVariable(null);

            if (checkItem != null && checkItem.item != null)
                return (WeaponItem)checkItem.item;
            return null;
        }
    }

    WeaponItem m_currentWeapon;
    WeaponItem m_previousWeapon;
    [SerializeField] GameObject WeaponSpawnLocation;
    GameObject SpawnedWeapon;

    UI_GUI uiInventory;

    bool isNotInProcessOfShooting = true;

    void WeaponChangeInVariable(InventorySquare checkItem)
    {

        //print("current weapon is diff");
        m_previousWeapon = m_currentWeapon;
        if (checkItem != null)
            m_currentWeapon = (WeaponItem)checkItem.item;
        else
            m_currentWeapon = null;
        WeaponChange();
    }

    void WeaponChange()
    {
        //print("weapon changing");
        if (m_currentWeapon == null)
        {
            if (m_previousWeapon == null) return;

            // Remove the weapon
            //print("removed the weapon");
            Destroy(SpawnedWeapon);
            return;
        }

        //print("spawning weapon");
        SpawnedWeapon = Instantiate(m_currentWeapon.ItemsToSpawn[0], WeaponSpawnLocation.transform, false);
        SpawnedWeapon.name = SpawnedWeapon.name.Replace("(Clone)", "");

        // Write code here to Instantiate the Weapon in
    }

    RaycastHit hit;

    void Start()
    {
        uiInventory = UI_GUI.instance;
    }

    void Update()
    {
        if (WeaponOnHand == null) return;
        if (!uiInventory.PlayerOnHandInventory.activeInHierarchy) return;

        // Left click
        if (Input.GetMouseButtonDown(0) && isNotInProcessOfShooting)
        {
            isNotInProcessOfShooting = false;
            WeaponOnHand.Use();
            WeaponUse();
        }

        // Right click
        if (Input.GetMouseButtonDown(1))
        {
            WeaponOnHand.RightClick();
        }
    }

    void WeaponUse()
    {
        if (WeaponOnHand.WeaponType == WeaponItem.Type.Blade)
        {
            //print("raycasting and damaging");
            StartCoroutine(nameof(RayCastOut));
        }
    }

    IEnumerator RayCastOut()
    {
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, WeaponOnHand.Distance))
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                hit.collider.GetComponent<EnemyHealth>().DeductHealth(WeaponOnHand.Damage);
                yield return new WaitForSeconds(WeaponOnHand.Delay);
            }
        }
        isNotInProcessOfShooting = true;
        yield return null;
    }
}
