﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerStamina : Stats
{
    PlayerStats localPlayerStats;
    FirstPersonController firstPersonController;

    float reductionPerSecondOfSprinting = 9f;
    float restorePerSecondOfNotSprinting = 3.5f;

    bool movementCheck;

    void Update()
    {
        if (UI_PlayerStats.instance.localPlayerStats != null && localPlayerStats == null)
        {
            localPlayerStats = UI_PlayerStats.instance.localPlayerStats;
            firstPersonController = localPlayerStats.GetComponent<FirstPersonController>();
        }

        movementCheck = Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0;

        if (Value > 0)
        {
            if (Input.GetKey(KeyCode.LeftShift) && movementCheck)
                Deduct();
            else if (Value != MaxValue)
                Restore();
        }
        else if (!(Value > 0) && !firstPersonController.IsStaminaEmpty)
            firstPersonController.IsStaminaEmpty = true;
        else if (!Input.GetKey(KeyCode.LeftShift) && firstPersonController.IsStaminaEmpty)
            Restore();

        if (Value > 0 && firstPersonController.IsStaminaEmpty)
            firstPersonController.IsStaminaEmpty = false;
    }

    void Deduct()
    {
        if (Value - (reductionPerSecondOfSprinting * Time.deltaTime) >= 0)
            Deduct(reductionPerSecondOfSprinting * Time.deltaTime);
        else if (Value - (reductionPerSecondOfSprinting * Time.deltaTime) < 0)
            Deduct(Value);

        if (localPlayerStats.c_Hunger.Value > 0)
            localPlayerStats.c_Hunger.Deduct(0.9f * Time.deltaTime);
    }

    void Restore()
    {
        float totalRestore = restorePerSecondOfNotSprinting * (movementCheck ? 1f : 1.8f);

        if (Value + (totalRestore * Time.deltaTime) <= MaxValue)
            Add(totalRestore * Time.deltaTime);
        else if (Value + (totalRestore * Time.deltaTime) > MaxValue)
            Add(MaxValue - Value);
    }
}
