﻿using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    // This script is to take in keyboard input and does stuff to the UI

    // UI_PlayerStats.instance.localPlayerStats
    // This can be used to access the player GO and everything Component in it

    UI_GUI uiGUI;
    UI_PlayerOnHandInventory uiOnHandInventory;

    void Start()
    {
        uiGUI = UI_GUI.instance;
        uiOnHandInventory = UI_PlayerOnHandInventory.instance;
    }

    void Update()
    {
        // WASD - Movement
        // Space - Jump
        // Mouse Movement - Player look

        // E - Inventory
        // Check for keyboard input to open/close UI
        // This is for own player's inventory

        if (uiGUI.PlayerOnHandInventory.activeInHierarchy && 
            (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
        {
            if (Input.GetMouseButtonDown(0))
                uiOnHandInventory.UseItemOnHand(0);
            else if (Input.GetMouseButtonDown(1))
                uiOnHandInventory.UseItemOnHand(1);
        }

        if (Input.GetKeyDown(KeyCode.E) && !uiGUI.PlayerInventorySharedViewGO.activeInHierarchy)
        {
            if (!uiGUI.BackgroundBlurGO.activeInHierarchy)
                uiGUI.OpenOwnInventory();
            else
                uiGUI.CloseOwnInventory();
        }

        if (Input.GetKeyDown(KeyCode.R) && !uiGUI.PlayerInventoryOwnGO.activeInHierarchy)
        {
            if (!uiGUI.BackgroundBlurGO.activeInHierarchy)
                uiGUI.OpenSharedViewInventory();
            else
                uiGUI.CloseSharedViewInventory();
        }

        // Escape - Pause Menu & Escape current menu
        // Close Inventory/Chest UI
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (uiGUI.BackgroundBlurGO.activeInHierarchy)
            {
                if (uiGUI.PlayerInventoryOwnGO.activeInHierarchy)
                    uiGUI.CloseOwnInventory();
                else if (uiGUI.PlayerInventorySharedViewGO.activeInHierarchy)
                    uiGUI.CloseSharedViewInventory();
                else if (uiGUI.ChestSmallGO.activeInHierarchy)
                    uiGUI.CloseSmallChestInventory();
                else if (uiGUI.ChestMediumGO.activeInHierarchy)
                    uiGUI.CloseMediumChestInventory();

                
            }
            else
            {
                if (uiGUI.EscapeMenu.activeInHierarchy)
                {
                    // Close both, just in case
                    uiGUI.ClosePauseMenu();
                    uiGUI.CloseSettingsMenu();

                    // Close Escape Menu
                    uiGUI.CloseEscapeMenu();

                    // Open background things
                    uiGUI.OpenPlayerOnHandInventory();
                }
                else
                {
                    // Close background things
                    uiGUI.ClosePlayerOnHandInventory();

                    // Open Escape Menu with Pause Menu within it
                    uiGUI.OpenEscapeMenu();

                    // Always open only the pause menu
                    uiGUI.OpenPauseMenu();
                }
            }
        }

        // Mouse click - Weapons/Combat

        // R - (When available) - Interacts with an object
    }
}
