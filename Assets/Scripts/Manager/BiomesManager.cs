﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BiomesManager : MonoBehaviour
{
    // This will spawn the biomes and manager the effects of each on the player

    public static BiomesManager instance;
    PlayerStats m_playerStats;

    // List of Biomes that is manually set
    // This contains the data for ALL the biomes in the map
    public List<Biome> Biomes = new List<Biome>();

    // The current biome will be displayed here
    public Biome CurrentBiome;

    // Toggles Constant Drain
    //bool IsConstantDrainOn = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Update()
    {
        // Constant drain on stats (when in any biome, except "warm" biomes)

        if (CurrentBiome == null) return;
        if (CurrentBiome.IsWarmBiome) return;
        if (CurrentBiome.IsSafe) return;
        //if (IsConstantDrainOn) return;

        if (m_playerStats == null) m_playerStats = UI_PlayerStats.instance.localPlayerStats;

        ConstantDrain();
        //StartCoroutine(nameof(ConstantDrain));
        // 5 second intervals
    }

    // 5 sec interval
    // 12 / min
    // 60 / 5 min
    // 120 / 10 min

    // NOTE: CONSTANT DRAIN DEPENDS ON WHAT 'CHARACTER' THAT IS CHOSEN
    // TO BE CHANGED IN THE FUTURE

    //IEnumerator ConstantDrain()
    //{
    //    IsConstantDrainOn = true;
    //    while(CurrentBiome != null)
    //    {
    //        yield return new WaitWhile(() => CurrentBiome.IsSafe);
    //        //print("constant drain");
    //        // Drains stats
    //        m_playerStats.c_Hunger.Deduct(-0.2f);
    //        m_playerStats.c_Warmth.Deduct(-0.3f);
    //        m_playerStats.c_Alertness.Deduct(-0.1f);
    //        yield return new WaitForSeconds(5f);
    //    }
    //    IsConstantDrainOn = false;
    //}

    void ConstantDrain()
    {
        if (!(m_playerStats.c_Health.Value > 0))
        {
            // Player is dead
            return;
        }

        if (m_playerStats.c_Hunger.Value > 0)
            m_playerStats.c_Hunger.Deduct(0.15f * Time.deltaTime);
        else if (!(m_playerStats.c_Hunger.Value > 0) && !(m_playerStats.c_Warmth.Value > 0) && m_playerStats.c_Health.Value > 0)
            m_playerStats.c_Health.Deduct(0.28f * Time.deltaTime);

        if (m_playerStats.c_Warmth.Value > 0)
            m_playerStats.c_Warmth.Deduct(0.2f * Time.deltaTime);
        else if (m_playerStats.c_Hunger.Value > 0)
            m_playerStats.c_Hunger.Deduct(0.09f * Time.deltaTime);

        if (m_playerStats.c_Alertness.Value > 0)
            m_playerStats.c_Alertness.Deduct(0.1f * Time.deltaTime);
    }
}
