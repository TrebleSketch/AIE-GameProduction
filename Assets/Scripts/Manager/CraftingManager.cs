﻿using UnityEngine;
using System.Collections.Generic;

public class CraftingManager : MonoBehaviour
{
    public static CraftingManager instance;

    [SerializeField] Transform CraftingButtonsArea;
    [SerializeField] GameObject ItemCraftingButtonGO;

    public Transform ObjectSpawningWorldAnchor;

    public List<Item> ItemsAllowed = new List<Item>();

    public List<Item> ItemsAbleToCrafted = new List<Item>();

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Start()
    {
        for (int buttons = 0; buttons < ItemsAbleToCrafted.Count; buttons++)
        {
            ItemToCraft button = Instantiate(ItemCraftingButtonGO, CraftingButtonsArea).GetComponent<ItemToCraft>();
            button.SetUp(ItemsAbleToCrafted[buttons]);
        }
    }
}
