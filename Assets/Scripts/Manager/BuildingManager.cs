﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.AI;

public class BuildingManager : MonoBehaviour
{
    public static BuildingManager instance;

    // This list will have all of the prefabs to be built in the world and its relating item
    public Dictionary<Item, GameObject> ItemsAbleToBeBuilt => itemsAbleToBeBuilt;
    Dictionary<Item, GameObject> itemsAbleToBeBuilt = new Dictionary<Item, GameObject>();

    // List of GameObjects
    [SerializeField] List<GameObject> itemsGameObjects = new List<GameObject>();
    // List of accompanying Items
    [SerializeField] List<Item> itemsData = new List<Item>();

    List<GameObject> m_itemsBuilt = new List<GameObject>();

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
        
        if (itemsGameObjects.Count != itemsData.Count)
        {
            Debug.LogWarning("The two lists are not the same length, please make sure that all item Gameobjects have an accompanying Item Data", this);
            return;
        }

        for (int itemsIndex = 0; itemsIndex < itemsData.Count; itemsIndex++)
        {
            itemsAbleToBeBuilt.Add(itemsData[itemsIndex], itemsGameObjects[itemsIndex]);
        }
    }

    public void BuildItem (Item item, Vector3 spawnLocation, Quaternion rotation)
    {
        // todo: generalize the spawning of items/models, as this is only written for the igloo
        // check if they are within 5x + 5y of the nearest igloo
        if (m_itemsBuilt.Count > 0)
        {
            List<float> positionsOfIgloos = new List<float>();
            for (int igloo = 0; igloo < m_itemsBuilt.Count; igloo++)
            {
                positionsOfIgloos.Add(Vector3.Distance(spawnLocation, m_itemsBuilt[igloo].transform.position));
            }

            if (positionsOfIgloos.Min(check => check < 6))
            {
                //Debug.Log("Too close, don't build");
                item.ShouldItemBeUsed = false;
                return;
            }
        }

        // search the list with "itemName" string
        GameObject spawnedObject = Instantiate(ItemsAbleToBeBuilt[item], CraftingManager.instance.ObjectSpawningWorldAnchor);
        // Update name
        spawnedObject.name = spawnedObject.name.Replace("(Clone)", "");
        // Then spawn it at "spawnLocation"
        //spawnedObject.transform.position = spawnLocation;

        // additional things from Sarexicus
        NavMeshHit hit;
        NavMesh.SamplePosition(spawnLocation, out hit, 5f, NavMesh.AllAreas);

        // Check for proper position to spawn
        spawnedObject.transform.position = hit.position != spawnLocation ? hit.position : spawnLocation;

        // Then give it a rotation
        spawnedObject.transform.rotation = rotation;

        m_itemsBuilt.Add(spawnedObject);
    }

    public void DemolishItem(Vector3 raycastPosition)
    {
        // Find the item closest to this point and then remove it
    }

    public Item DemolishItemAndGetItem(Vector3 raycastPosition)
    {
        // Find the item closest to this point and then remove it

        // Then return an item to be given back to the player
        return null;
    }
}
