﻿using UnityEngine;

[RequireComponent(typeof(MapGeneration))]
public class MapManager : MonoBehaviour
{
    public static MapManager instance;
    MapGeneration m_MapGeneration;

    // int16
    readonly short seed_preset1 = 32764;
    //readonly short seed_preset2 = 18382;
    //readonly short seed_preset3 = 2812;

    short seed;
    public short Seed => seed;

    // Assets for generation

    // Generates a seed first
    // Create a map out of the seed

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        // Make the seed here please
        seed = seed_preset1;
        Noise.Seed = seed;
    }

    void Start ()
    {
        m_MapGeneration = MapGeneration.instance;

        // Generate the map here please
        m_MapGeneration.GenerateMap(seed);
	}
}
