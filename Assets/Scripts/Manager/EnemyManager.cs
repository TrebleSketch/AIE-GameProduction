﻿using UnityEngine;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour
{
    public enum Enemies
    {
        None,
        Notkea,
        Atreyu,
        Var
    }

    public static EnemyManager instance;

    [SerializeField] List<GameObject> ListOfEnemiesPrefabs = new List<GameObject>();
    public Dictionary<Enemies, GameObject> EnemyPrefabs = new Dictionary<Enemies, GameObject>();

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        for (int prefabs = 0; prefabs < ListOfEnemiesPrefabs.Count; prefabs++)
        {
            EnemyPrefabs.Add((Enemies)prefabs+1, ListOfEnemiesPrefabs[prefabs]);
        }
    }
}
