﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class UI_GUI : MonoBehaviour
{
    // Clipsey idea
    // Then make an Inventory renderer class that via
    // unity UI system builds an inventory UI programmatically
    // and then puts icons etc in to it

    public static UI_GUI instance;

    public Item MouseHoldingItem;


    //Inventory[] InventoriesArray = new Inventory[6];

    [SerializeField] GameObject playerOnHandInventory;
    public GameObject PlayerOnHandInventory => playerOnHandInventory;

    [SerializeField] GameObject backgroundBlur;
    public GameObject BackgroundBlurGO => backgroundBlur;

    // Player/s inventory
    [SerializeField] GameObject playerInventoryOwn;
    [SerializeField] GameObject playerInventorySharedView;
    public GameObject PlayerInventoryOwnGO => playerInventoryOwn;
    public GameObject PlayerInventorySharedViewGO => playerInventorySharedView;

    // Chests
    [SerializeField] GameObject chestSmall;
    [SerializeField] GameObject chestMedium;
    public GameObject ChestSmallGO => chestSmall;
    public GameObject ChestMediumGO => chestMedium;

    // Item
    [SerializeField] GameObject itemFloatingIconGO;
    Image itemFloatingIcon;

    // Escape Menu
    [SerializeField] GameObject escapeMenu;
    public GameObject EscapeMenu => escapeMenu;

    // Pause Menu
    [SerializeField] GameObject pauseMenu;
    public GameObject PauseMenu => pauseMenu;

    // Settings Menu
    [SerializeField] GameObject settingsMenu;
    public GameObject SettingsMenu => settingsMenu;

    // Inventory Info Popup
    [SerializeField] GameObject inventorySquareInfoPopup;
    public GameObject InventorySquareInfoPopup => inventorySquareInfoPopup;



    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        itemFloatingIcon = itemFloatingIconGO.GetComponent<Image>();
    }

    void LateUpdate()
    {
        if (itemFloatingIcon.sprite == null) return;
        itemFloatingIconGO.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
    }



    
    public void TurnOnBackgroundBlur()
    {
        backgroundBlur.SetActive(true);
        ClosePlayerOnHandInventory();
    }

    public void TurnOffBackgroundBlur()
    {
        backgroundBlur.SetActive(false);
        OpenPlayerOnHandInventory();
    }




    public void OpenPlayerOnHandInventory()
    {
        SetCursorAsNotVisible();
        SetPlayerMovementAsMoveable();
        playerOnHandInventory.SetActive(true);
    }

    public void ClosePlayerOnHandInventory()
    {
        playerOnHandInventory.SetActive(false);
        SetCursorAsVisible();
        SetPlayerMovementAsImmovable();
    }




    public void OpenOwnInventory()
    {
        TurnOnBackgroundBlur();
        playerInventoryOwn.SetActive(true);
    }

    public void CloseOwnInventory()
    {
        playerInventoryOwn.SetActive(false);
        TurnOffBackgroundBlur();
    }




    public void OpenSharedViewInventory()
    {
        TurnOnBackgroundBlur();
        playerInventorySharedView.SetActive(true);
    }

    public void CloseSharedViewInventory()
    {
        playerInventorySharedView.SetActive(false);
        TurnOffBackgroundBlur();
    }




    public void OpenSmallChestInventory()
    {
        TurnOnBackgroundBlur();
        chestSmall.SetActive(true);
    }

    public void CloseSmallChestInventory()
    {
        chestSmall.SetActive(false);
        TurnOffBackgroundBlur();
    }




    public void OpenMediumChestInventory()
    {
        TurnOnBackgroundBlur();
        chestMedium.SetActive(true);
    }

    public void CloseMediumChestInventory()
    {
        chestMedium.SetActive(false);
        TurnOffBackgroundBlur();
    }




    public void OpenEscapeMenu()
    {
        escapeMenu.SetActive(true);
    }

    public void CloseEscapeMenu()
    {
        escapeMenu.SetActive(false);
    }

    
    
    
    
    public void OpenPauseMenu()
    {
        pauseMenu.SetActive(true);
    }
    
    public void ClosePauseMenu()
    {
        pauseMenu.SetActive(false);
    }




    public void OpenSettingsMenu()
    {
        settingsMenu.SetActive(true);
    }

    public void CloseSettingsMenu()
    {
        settingsMenu.SetActive(false);
    }




    public void UpdateFloatingIcon(Sprite icon)
    {
        itemFloatingIcon.sprite = icon;
        itemFloatingIcon.color = icon != null ? Color.white : new Color(1, 1, 1, 0);
    }




    public void EndGame()
    {
        Debug.Log("Ending game in 1 sec...");
        Invoke(nameof(ActuallyEnd), 1);
    }

    void ActuallyEnd()
    {
        Debug.Log("Ending game now...");
        Application.Quit();
    }



    // This is a temporary solution until a new Player Movement script is written
    void SetPlayerMovementAsMoveable()
    {
        UI_PlayerStats.instance.localPlayerStats.GetComponent<FirstPersonController>().enabled = true;
    }

    void SetPlayerMovementAsImmovable()
    {
        UI_PlayerStats.instance.localPlayerStats.GetComponent<FirstPersonController>().enabled = false;
    }

    void SetCursorAsVisible()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void SetCursorAsNotVisible()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }



    public void OpenSite(string url)
    {
        Application.OpenURL(url);
        Debug.Log("User has opened a site - " + url);
    }
}
