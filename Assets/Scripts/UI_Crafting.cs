﻿using UnityEngine;

public class UI_Crafting : MonoBehaviour
{
    public static UI_Crafting instance;

    public InventorySquare CraftingOutput => CraftingItemOutputGO.GetComponent<InventorySquare>();
    [SerializeField] GameObject CraftingItemOutputGO;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

}
