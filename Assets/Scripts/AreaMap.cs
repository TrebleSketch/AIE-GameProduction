﻿using System;
using UnityEngine;

public class AreaMap : MonoBehaviour
{
    Transform localPlayerTransform;

    void Start()
    {
        localPlayerTransform = GameObject.FindWithTag("Player") != null ? GameObject.FindWithTag("Player").transform : Camera.main.gameObject.transform;
    }

    void LateUpdate()
    {
        if (localPlayerTransform != null)
            transform.position = new Vector3(localPlayerTransform.position.x, localPlayerTransform.position.y + 20f, localPlayerTransform.position.z);
    }
}
