﻿using UnityEngine;
using UnityEngine.UI;

public class UI_ArmourAttire : MonoBehaviour
{
    public static UI_ArmourAttire instance;

    public ArmourInventorySquare Head; // Updates PlayerArmour
    public ArmourInventorySquare Body; // Updates PlayerArmour (and maybe to PlayerStats too)
    public ArmourInventorySquare LeftWeapon; // Updates "PlayerWeapon" (to be created)
    public ArmourInventorySquare RightWeapon; // Updates "PlayerWeapon"
    public ArmourInventorySquare Legs; // Updates PlayerArmour

    [SerializeField] Text ArmourStatsText;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Start()
    {
        // Update base armour stat
        ArmourStatsText.text = "Armour:\n" + UI_PlayerStats.instance.localPlayerStats.c_Armour.BaseValue;
    }

    // Self updates the Armour and weapon stats when placed inside either one of these InventorySquares
    public void UpdatePlayerArmourStats()
    {
        if (ArmourStatsText != null)
        {
            int totalValue = UI_PlayerStats.instance.localPlayerStats.c_Armour.BaseValue;

            if (Head.item != null)
            {
                ArmourItem HeadArmourStats = (ArmourItem)Head.item;
                totalValue += HeadArmourStats.GetArmourValue();
            }
            if (Body.item != null)
            {
                ArmourItem BodyArmourStats = (ArmourItem)Body.item;
                totalValue += BodyArmourStats.GetArmourValue();
            }
            if (Legs.item != null)
            {
                ArmourItem LegsArmourStats = (ArmourItem)Legs.item;
                totalValue += LegsArmourStats.GetArmourValue();
            }
            
            UI_PlayerStats.instance.localPlayerStats.c_Armour.UpdateArmourValue(totalValue);
            ArmourStatsText.text = "Armour:\n" + totalValue;
        }
        else
            Debug.LogWarning("Armour Stats Text UI isn't set!", this);
    }
}
