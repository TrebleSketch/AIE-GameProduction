﻿using UnityEngine;
using System;

[Serializable]
public class Biome : MonoBehaviour
{
    [Serializable]
    public enum BiomeTypes
    {
        None,
        // Winter theme
        Tundra,
        SnowyMountains,
        Permafrost,
        Blizzard
    }

    public BiomeTypes BiomeType = BiomeTypes.None;
    BiomesManager manager;

    // Biome Data
    public string Name = "New Biome";
    public string Description = "Biome Description";
    public bool IsWarmBiome;
    public bool IsSafe;
    public bool IsRunning;

    // Biome Collider
    public Collider BiomeCollider;

    // Variable to Player's Stats
    protected PlayerStats PlayersStats => UI_PlayerStats.instance.localPlayerStats;

    void Start()
    {
        manager = BiomesManager.instance;
    }

    // Biome Effects on the player
    // Effects are inflicted every second
    public virtual void Effect()
    {
        // What logic does every effect need?
        // todo: If player is inside a structure, this effect will be nulled until they are "outside" again
        // This is checked for later...
        // NOTE: BiomesManager will handle this

        //if (IsSafe) return;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        manager.CurrentBiome = this;

        // Once Entered, start affecting player with the effects

        IsRunning = true;

        // 1 sec interval
        // 60 / min
        // 300 / 5 min
        // 600 / 10 min

        // 4 sec interval
        // 15 / min
        // 75 / 5 min
        // 150 / 10 min

        // 5 sec interval
        // 12 / min
        // 60 / 5 min
        // 120 / 10 min

        //Debug.Log("Blep, collision Enter: " + gameObject.name + ". Colliding with: " + other.name, this);
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        manager.CurrentBiome = null;

        IsRunning = false;

        // When leaving a biome, effects will be stopped
        //CancelInvoke(nameof(Effect));
    }

    void Update()
    {
        if (IsRunning)
            Effect();
    }
}
