﻿using UnityEngine;

public class TundraBiome : Biome
{
    void Awake()
    {
        BiomeType = BiomeTypes.Tundra;
    }

    // How do I set the data for this specific biome?

    // This is called every 5 seconds

    public override void Effect()
    {
        // Checks for if the player is "safe" within the biome
        // This includes if they are inside a structure that is shielding them from the environment
        //base.Effect();

        if (IsSafe) return;

        //print("tundra drain");

        // Tundra effects

        // 5 sec interval
        // 12 / min
        // 60 / 5 min
        // 120 / 10 min

        // CONSTANT DRAIN
        // Slowly wears out the player's warmth and eats on their hunger
        //PlayersStats.c_Hunger.Deduct(0.1f);
        if (PlayersStats.c_Warmth.Value > 0)
            PlayersStats.c_Warmth.Deduct(0.085f * Time.deltaTime);
        if (PlayersStats.c_Alertness.Value > 0)
            PlayersStats.c_Alertness.Deduct(0.09f * Time.deltaTime);
    }
}
