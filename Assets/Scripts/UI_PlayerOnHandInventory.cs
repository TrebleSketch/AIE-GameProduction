﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UI_PlayerOnHandInventory : MonoBehaviour
{
    public static UI_PlayerOnHandInventory instance;

    public InventorySquare CurrentSelection => m_currentSelection;
    [SerializeField] InventorySquare m_currentSelection;

    public InventorySquare CurrentWeaponSelection => m_currentWeaponSelection;
    [SerializeField] InventorySquare m_currentWeaponSelection;

    [SerializeField] GameObject backgroundBorder;

    [SerializeField] Inventory onHandInventory;
    public Inventory WeaponInventory => weaponInventory;
    [SerializeField] Inventory weaponInventory;

    List<InventorySquare> inventorySquares = new List<InventorySquare>();

    // Inventory 1 - 0
    // Two weapons Q toggles between

    int m_scroll = 3;
    int m_previousScroll = -1;
    readonly Color originalColor = new Color(212f/255f, 1, 251f/255f);

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Start()
    {
        for (int squares = 0; squares < (onHandInventory.InventorySquares.Count + weaponInventory.InventorySquares.Count); squares++)
        {
            if (squares < 2)
                inventorySquares.Add(weaponInventory.InventorySquares[squares]);
            else
                inventorySquares.Add(onHandInventory.InventorySquares[squares - 2]);
        }

        if (inventorySquares[0].transform.position == inventorySquares[6].transform.position)
            Debug.LogWarning("Please make sure to disable the 'Grid Layout Group' or else this won't work. Thank you.");

        BackgroundBorderPositioning();
    }

    public void UseItemOnHand(uint buttonPressed)
    {
        if (m_currentSelection != null)
            m_currentSelection.UseItem(buttonPressed);
    }

    public bool GivePlayerItemAndAnIsAbleCheck(Item item)
    {
        for (int i = 2 ; i < onHandInventory.InventorySquares.Count; i++)
        {
            if (inventorySquares[i].item == null)
            {
                inventorySquares[i].GivePlayerItemFromAction(item);
                // Once done, leave
                return true;
            }
        }
        return false;
    }

    void Update()
    {
        if (Math.Abs(Input.GetAxis("Mouse ScrollWheel")) > 0.01f)
        {
            m_previousScroll = m_scroll;

            // Logic from "Understanding MouseScroll Wheel Input" on Unity Forums
            // Source: https://forum.unity.com/threads/understanding-mousescroll-wheel-input.414523/#post-2702746
            // "-=" This makes scrolling down goes to the right
            // "+=" This makes scrolling down goes to the left
            m_scroll -= Mathf.RoundToInt(Input.GetAxis("Mouse ScrollWheel") * 10);

            // Wrap between 0 and 9
            if (m_scroll > 9)
                m_scroll = 0;
            else if (m_scroll < 0)
                m_scroll = 9;
            //m_scroll = Mathf.Clamp(m_scroll, 0, 9); //prevents value from exceeding specified range
            BackgroundBorderPositioning();
        }
        
        // if no keys are pressed, return
        if (!Input.anyKeyDown) return;

        // If no number keys are pressed, meh
        if (!Input.GetKeyDown(KeyCode.Alpha1) &&
            !Input.GetKeyDown(KeyCode.Alpha2) &&
            !Input.GetKeyDown(KeyCode.Alpha3) &&
            !Input.GetKeyDown(KeyCode.Alpha4) &&
            !Input.GetKeyDown(KeyCode.Alpha5) &&
            !Input.GetKeyDown(KeyCode.Alpha6) &&
            !Input.GetKeyDown(KeyCode.Alpha7) &&
            !Input.GetKeyDown(KeyCode.Alpha8) &&
            !Input.GetKeyDown(KeyCode.Alpha9) &&
            !Input.GetKeyDown(KeyCode.Alpha0))
            return;

        m_previousScroll = m_scroll;

        // If so, take note of what was pressed and then update
        if (Input.GetKeyDown(KeyCode.Alpha1))
            m_scroll = 1;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            m_scroll = 2;
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            m_scroll = 3;
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            m_scroll = 4;
        else if (Input.GetKeyDown(KeyCode.Alpha5))
            m_scroll = 5;
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            m_scroll = 6;
        else if (Input.GetKeyDown(KeyCode.Alpha7))
            m_scroll = 7;
        else if (Input.GetKeyDown(KeyCode.Alpha8))
            m_scroll = 8;
        else if (Input.GetKeyDown(KeyCode.Alpha9))
            m_scroll = 9;
        else if (Input.GetKeyDown(KeyCode.Alpha0))
            m_scroll = 0;

        BackgroundBorderPositioning();
    }

    // This sets the background border to be behind either one of the inventory boxes.
    void BackgroundBorderPositioning()
    {
        // Because of the layout script, has to manually make sure that 1-9 is on the left while 0 is on the very right.
        int wrapChecks = m_scroll;

        if (wrapChecks != 0)
            wrapChecks--;
        else if (wrapChecks == 0)
            wrapChecks = 9;

        //Debug.Log(m_previousScroll);

        if (m_previousScroll != -1)
        {
            int previousSquare = m_previousScroll;

            if (previousSquare != 0)
                previousSquare--;
            else if (previousSquare == 0)
                previousSquare = 9;

            inventorySquares[previousSquare].GetComponent<Image>().color = originalColor;
        }

        backgroundBorder.transform.position = inventorySquares[wrapChecks].transform.position;
        Color modifiedColour = originalColor;
        modifiedColour *= 0.8f;
        inventorySquares[wrapChecks].GetComponent<Image>().color = modifiedColour;


        if (wrapChecks == 0 || wrapChecks == 1)
        {
            m_currentWeaponSelection = inventorySquares[wrapChecks];
            m_currentSelection = null;
            return;
        }

        m_currentWeaponSelection = null;
        m_currentSelection = inventorySquares[wrapChecks];
    }
}
