﻿using UnityEngine;

public class MapGeneration : MonoBehaviour
{
    // Make sure that this script is located in the root gameobject of the Map

    // This will be in the root Gameobject of the map
    public static MapGeneration instance;

    // Map Seed
    //short seed;

    // Width/Length of the map
    //int sizeX;
    //int sizeZ;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        // Creation of the terrain (from prefab)

    }

    public void GenerateMap (short mapSeed)
    {
        Debug.Log("Start Map Generation!");
        //seed = mapSeed;
        Invoke(nameof(InternalGenerateMap), 0);
    }

    void InternalGenerateMap()
    {
        // Set size
        //sizeX = 5;
        //sizeZ = 5;

        // Create a terrain map from seed
        // Terrain-map based
        //gameObject.AddComponent<Terrain>();
        //gameObject.AddComponent<TerrainCollider>();



        // Square based
        //for (int x = 0; x < sizeX; x++)
        //{
        //    for (int z = 0; z < sizeZ; z++)
        //    {
        //        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //        plane.transform.parent = gameObject.transform;
        //        // Times 10 due to it being a plane
        //        // TImes 100 would be for a terrain
        //        plane.transform.position = new Vector3(x * 10, 0, z * 10);
        //        plane.name = $"Plane [{x}, 0, {z}]";
        //    }
        //}
        Debug.Log("Finish creating terrain!");

        // Create trees/bushes/rocks


        // Create spawn points

    }
}
