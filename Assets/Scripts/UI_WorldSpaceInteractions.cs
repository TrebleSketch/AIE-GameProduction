﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class UI_WorldSpaceInteractions : MonoBehaviour
{
    public static UI_WorldSpaceInteractions instance;

    public enum UIType
    {
        Tree
    }

    public List<GameObject> UIGOList = new List<GameObject>();
    Dictionary<UIType, GameObject> UIList = new Dictionary<UIType, GameObject>();
    List<Transform> billboards = new List<Transform>();

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        // NOTE: Please put the Inventory UI in the same order as the enum

        for (int i = 0; i < UIGOList.Count; i++)
        {
            UIList.Add((UIType)i, UIGOList[i]);
        }
    }

    public GameObject GenerateUI(UIType typeToSpawn, Vector3 positionToSpawn)
    {
        // create a new UI
        //GameObject newUI = Instantiate(UIList[typeToSpawn], positionToSpawn, Quaternion.identity, transform);
        GameObject newUI = Instantiate(UIList[typeToSpawn], transform, true);
        newUI.name = newUI.name.Replace("(Clone)", "");
        newUI.transform.position = positionToSpawn;

        // Add it to the billboard list
        billboards.Add(newUI.transform);
        return newUI;
    }

    public void RemoveUI(GameObject uiToRemove)
    {
        // Check if it exists
        if (!billboards.Exists(obj => uiToRemove.transform)) return;

        // remove from the billboard list
        billboards.Remove(uiToRemove.transform);

        // Then actually destroy it
        Destroy(uiToRemove);
    }

    
    void LateUpdate()
    {
        if (billboards.Count == 0) return;

        foreach (Transform billboard in billboards)
        {
            // http://wiki.unity3d.com/index.php?title=CameraFacingBillboard
            // Simple script, this should make it always point at the player
            billboard.transform.LookAt(billboard.transform.position + Camera.main.transform.rotation * Vector3.forward,
            Camera.main.transform.rotation * Vector3.up);
        }
    }
}
