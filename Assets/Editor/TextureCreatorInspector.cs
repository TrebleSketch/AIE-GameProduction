﻿using UnityEditor;
using UnityEngine;

// Source: https://catlikecoding.com/unity/tutorials/noise/

[CustomEditor(typeof(TextureCreator))]
public class TextureCreatorInspector : Editor
{
    private TextureCreator creator;
    void OnEnable ()
    {
        creator = target as TextureCreator;
        Undo.undoRedoPerformed += RefreshCreator;
    }

    void OnDisable ()
    {
        Undo.undoRedoPerformed -= RefreshCreator;
    }

    void RefreshCreator ()
    {
        if (Application.isPlaying)
        {
            creator.FillTexture();
        }
    }

    public override void OnInspectorGUI ()
    {
        EditorGUI.BeginChangeCheck();
        DrawDefaultInspector();
        if (EditorGUI.EndChangeCheck())
        {
            RefreshCreator();
        }
    }
}